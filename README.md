# Simple Social Network  
project for training personal skills  
  
**Functionality**  
- account registration  
- authentication  
- display profile  
- edit profile  
- input validation  
- ajax autocomplete and search with pagination  
- upload and download avatar  
- users personal data export to xml  
- add/remove friends  
- common chat  
  
**Tools**  
JDK 8, Lombok,  
Spring Boot/Security/Data/Web/Rest/Test/DevTools/Websocket,  
JPA2 / Hibernate,  
PostgreSQL / H2 / MongoDB,  
JUnit / Mockito, Integration tests,  
JAXB, Jackson  
Slf4j / Logback,  
html, css, Bootstrap 3, jsp / jstl, jQuery / ajax, webjars, websocket  
Maven, Git / Bitbucket, Tomcat 8, IntelliJIDEA.  
  
**Notes**  
scripts for DB creation is located in the `dao/src/resources`  
`mongo_DB_prepare.js, postgres_DB_prepare.sql`  
  
**Screenshots**  
[link](https://bitbucket.org/Alexander_Mamaev/simplesocialnetwork/commits/462a99448a58b7bda72fe6c1e842e67aebec682a#chg-web/src/test/resources/screenshots/autocomplete.png)  
  
**Application on Heroku Cloud Platform**  
www.mamaevas.ru  
`login_1: email@mail.ru`  
`login_2: hue@mail.ru`  
`password: 12345678`  
  
**App start**  
`server.contextPath=/mamaevas`  
`server.port=5000`  
for other properties see application.properties  
  
  
Java Web Project, December 2016  
_  
**Mamaev Alexander**  
Training getJavaJob  
http://www.getjavajob.com  