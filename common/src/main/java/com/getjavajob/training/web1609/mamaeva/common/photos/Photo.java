package com.getjavajob.training.web1609.mamaeva.common.photos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

/**
 * Photo class represents picture and saved into the mongoDB within PHOTOS collection
 */
@Document(collection = "PHOTOS")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"name", "folderName"})
public class Photo {
    @Id
    @JsonIgnore
    private ObjectId id;
    private String name;
    private String folderName;
    private String description;
    private Long uploadTime;
    @DBRef
    private Account account;
    private byte[] photo;
}
