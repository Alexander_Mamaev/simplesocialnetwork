package com.getjavajob.training.web1609.mamaeva.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 * EntityId class used where the entities have id
 */
@MappedSuperclass
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class EntityId {
    @XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
}
