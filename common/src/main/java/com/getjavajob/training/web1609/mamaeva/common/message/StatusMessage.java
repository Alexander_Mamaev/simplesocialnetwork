package com.getjavajob.training.web1609.mamaeva.common.message;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;

public enum StatusMessage {
    SOMETHING_WRONG(900, "Something wrong"),
    SUCCESS(901, "Success"),
    AUTH_FAILED(902, "Authentication failed"),
    INCORRECT_DATA(903, "Incorrect data"),
    ERROR(904, "Error occurred"),
    EMAIL_EXISTS(905, "Email exists"),
    WRONG_PASSWORD(906, "Wrong password"),
    NOT_REQUIRED(907, "Not required"),
    NOT_MATCHED(908, "Not matched");

    private final int status;
    private final String message;

    StatusMessage(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    @JsonValue
    public StatusTuple toValue() {
        return new StatusTuple(status, message);
    }

    @Override
    public String toString() {
        return "StatusMessage{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(toValue());
    }

    @AllArgsConstructor
    @Getter
    private class StatusTuple {
        private final int status;
        private final String message;
    }
}
