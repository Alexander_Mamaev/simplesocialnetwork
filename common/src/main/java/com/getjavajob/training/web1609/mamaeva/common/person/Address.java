package com.getjavajob.training.web1609.mamaeva.common.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * Relation between AddressSimple and Person
 */
@Entity
@IdClass(PersonAddressKey.class)
@Table(name = "PERSON_ADDRESS")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(exclude = "description", callSuper = false)
@ToString(exclude = {"person"})
@XmlRootElement
@XmlType(propOrder = {"addressSimple", "description"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Address implements Serializable {
    @XmlTransient
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    @XmlTransient
    @JsonIgnore
    @Id
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id")
    private Person person;
    @Id
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "simple_address_id")
    @XmlElement(name = "addressSimple")
    private AddressSimple addressSimple;
    private String description;
}
