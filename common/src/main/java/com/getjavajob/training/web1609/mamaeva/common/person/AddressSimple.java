package com.getjavajob.training.web1609.mamaeva.common.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Physical address
 */
@Entity
@Table(name = "SIMPLE_ADDRESSES",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"country", "region", "city", "street", "house", "apartment"})
        })
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(exclude = {"addresses"}, callSuper = false)
@ToString(exclude = {"addresses"})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@XmlRootElement(name = "addressSimple")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressSimple extends EntityId implements Serializable, Cloneable {
    @XmlTransient
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ\\s]+$")
    private String country;
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ0-9\\s]+$")
    private String region;
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ0-9\\s]+$")
    private String city;
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ0-9\\s]+$")
    private String street;
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ0-9\\s/]+$")
    private String house;
    @Size(min = 1, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ0-9\\s/]+$")
    private String apartment;
    @XmlTransient
    @JsonIgnore
    @Singular
    @OneToMany(mappedBy = "addressSimple", fetch = FetchType.LAZY)
    private List<Address> addresses;

    /**
     * @return copy of this object aka object.clone()
     * @throws RuntimeException when CloneNotSupportedException occurs
     */
    public AddressSimple copy() throws RuntimeException {
        try {
            return (AddressSimple) this.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
