package com.getjavajob.training.web1609.mamaeva.common.dto;

import com.getjavajob.training.web1609.mamaeva.common.person.Contact;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.util.List;

/**
 * Data transfer object for contacts
 */
@NoArgsConstructor
@Data
public class ContactDTO {
    @Valid
    private List<Contact> contacts;
    private String personEmail;
}
