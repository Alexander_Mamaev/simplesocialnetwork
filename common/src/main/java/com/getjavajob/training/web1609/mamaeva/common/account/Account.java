package com.getjavajob.training.web1609.mamaeva.common.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Account entity that uses for authorization
 */
@Document(collection = "accounts")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"username"})
@ToString(of = "username")
public class Account implements UserDetails {
    private static final long serialVersionUID = 1L;
    @Id
    @JsonIgnore
    private ObjectId id;
    @Indexed(unique = true)
    private String username; //aka email
    private String password;
    @Singular
    private Set<Role> authorities;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    private LocalDate registrationDate;
    private LocalDateTime lastVisited;
}
