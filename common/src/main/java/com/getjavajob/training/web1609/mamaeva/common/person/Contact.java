package com.getjavajob.training.web1609.mamaeva.common.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Contacts of the persons
 */
@Entity
@Table(name = "CONTACTS")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(exclude = {"person"}, callSuper = false)
@ToString(exclude = {"person"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact extends EntityId implements Serializable {
    @XmlTransient
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    @Size(min = 1, max = 255)
    @Column(nullable = false)
    private String value;
    @Size(min = 1, max = 255)
    @Column(nullable = false)
    private String type;
    @XmlTransient
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id")
    private Person person;
}
