package com.getjavajob.training.web1609.mamaeva.common.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

/**
 * ChatMessage that saves into mongoDB
 */
@Document(collection = "chat_messages")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"id"})
public class ChatMessage {
    @Id
    @JsonIgnore
    private ObjectId id;
    @DBRef
    private Account account;
    private String message;
    private long time;
}
