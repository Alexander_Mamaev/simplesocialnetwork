package com.getjavajob.training.web1609.mamaeva.common.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Adapter for transfer new lines of String format to xml format and vice versa
 */
public class JaxbNewLineTextAdapter extends XmlAdapter<String, String> {
    @Override
    public String unmarshal(String v) throws Exception {
        return v.replaceAll("<br/>", System.lineSeparator());
    }

    @Override
    public String marshal(String v) throws Exception {
        return v.replaceAll("\r?\n", "<br/>");
    }
}
