package com.getjavajob.training.web1609.mamaeva.common.dto;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Phone;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Data transfer object for phone
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"number"})
@ToString
public class PhoneDTO {
    private long id;
    @NotNull
    @Size(min = 2, max = 255)
    private String description;
    @NotNull
    @Pattern(regexp = "^\\+\\d{11}")
    private String number;
    private long personId;
    private String personEmail;

    public Phone getPhone() {
        Person person = new Person();
        person.setId(personId);
        person.setEmail(personEmail);
        Phone phone = Phone.builder()
                .description(description)
                .number(number)
                .person(person)
                .build();
        phone.setId(id);
        return phone;
    }
}
