package com.getjavajob.training.web1609.mamaeva.common.account;

import org.springframework.security.core.GrantedAuthority;

/**
 * The authority role
 */
public enum Role implements GrantedAuthority {
    ROLE_USER, ROLE_ADMIN, ROLE_ANONYMOUS;

    @Override
    public String getAuthority() {
        return name();
    }
}
