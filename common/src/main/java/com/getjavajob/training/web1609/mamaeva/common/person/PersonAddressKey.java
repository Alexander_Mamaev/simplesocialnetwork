package com.getjavajob.training.web1609.mamaeva.common.person;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Custom composite key for Address.class
 */
@NoArgsConstructor
@Data
public class PersonAddressKey implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long person;
    private Long addressSimple;
}
