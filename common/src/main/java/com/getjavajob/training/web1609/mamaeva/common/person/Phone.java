package com.getjavajob.training.web1609.mamaeva.common.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Represent phone numbers and it relation to the persons
 */
@Entity
@Table(name = "PHONES")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"number"}, callSuper = false)
@ToString(exclude = {"person"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Phone extends EntityId implements Serializable {
    @XmlTransient
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    @Column(nullable = false)
    private String number;
    @Column(nullable = false)
    private String description;
    @XmlTransient
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id")
    private Person person;
}
