package com.getjavajob.training.web1609.mamaeva.common.dto;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.account.Role;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Sex;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Data transfer object for combined Account and Person
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"email"})
@ToString
public class AccountPersonDTO {
    @NotNull
    @Size(min = 2, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ\\s]+$")
    private String firstName;
    @NotNull
    @Size(min = 2, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ\\s]+$")
    private String lastName;
    @NotNull
    @Size(min = 2, max = 255)
    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ\\s]+$")
    private String patronymic;
    @NotNull
    @Pattern(regexp = "^(?:[a-zA-Z0-9_'^&/+-])+(?:\\.(?:[a-zA-Z0-9_'^&/+-])+)" +
            "*@(?:(?:\\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\\.)" +
            "{3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\]?)|(?:[a-zA-Z0-9-]+\\.)" +
            "+(?:[a-zA-Z]){2,}\\.?)$", message = "incorrect email")
    private String email;
    @NotNull
    private Sex sex;
    @NotNull
    private LocalDate birthday;
    @NotNull
    @Size(min = 8, max = 255)
    private String password;
    @NotNull
    @Size(min = 8, max = 255)
    private String confirmPassword;

    public Account getAccount() {
        return Account.builder()
                .username(email)
                .password(password)
                .authority(Role.ROLE_USER)
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .registrationDate(LocalDate.now())
                .build();
    }

    public Person getPerson() {
        return Person.builder()
                .firstName(firstName)
                .lastName(lastName)
                .patronymic(patronymic)
                .email(email)
                .birthday(birthday)
                .sex(sex)
                .build();
    }
}
