package com.getjavajob.training.web1609.mamaeva.common.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import com.getjavajob.training.web1609.mamaeva.common.adapters.JaxbLocalDateAdapter;
import com.getjavajob.training.web1609.mamaeva.common.adapters.JaxbNewLineTextAdapter;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Person entity that contains personal data and peron relationships
 */
@Entity
@Table(name = "PERSONS")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"email"}, callSuper = false)
@ToString(exclude = {"friends", "outcomeFriendRequest", "incomeFriendRequest"})
@XmlRootElement(name = "person")
@XmlAccessorType(XmlAccessType.FIELD)
public class Person extends EntityId implements Serializable {
    @XmlTransient
    @JsonIgnore
    private static final long serialVersionUID = 1L;
    @XmlElementWrapper(name = "contacts")
    @XmlElement(name = "contact")
    @JsonIgnore
    @Singular
    @OneToMany(mappedBy = "person", fetch = FetchType.LAZY, orphanRemoval = true)
    private final List<Contact> contacts = new ArrayList<>();
    @XmlElementWrapper(name = "phones")
    @XmlElement(name = "phone")
    @JsonIgnore
    @Singular
    @OneToMany(mappedBy = "person", fetch = FetchType.LAZY, orphanRemoval = true)
    private final List<Phone> phones = new ArrayList<>();
    @XmlElementWrapper(name = "addresses")
    @XmlElement(name = "address")
    @JsonIgnore
    @Singular
    @OneToMany(mappedBy = "person", fetch = FetchType.LAZY, orphanRemoval = true)
    private final List<Address> addresses = new ArrayList<>();
    @XmlTransient
    @JsonIgnore
    @Singular
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "PERSON_FRIEND",
            joinColumns = {@JoinColumn(name = "person_id")},
            inverseJoinColumns = {@JoinColumn(name = "friend_id")})
    private final List<Person> friends = new ArrayList<>();
    @XmlTransient
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "FRIEND_REQUEST",
            joinColumns = {@JoinColumn(name = "outcome_id")},
            inverseJoinColumns = {@JoinColumn(name = "income_id")})
    private final List<Person> outcomeFriendRequest = new ArrayList<>();
    @XmlTransient
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "FRIEND_REQUEST",
            joinColumns = {@JoinColumn(name = "income_id")},
            inverseJoinColumns = {@JoinColumn(name = "outcome_id")})
    private final List<Person> incomeFriendRequest = new ArrayList<>();
    @XmlTransient
    @JsonIgnore
    @Singular
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "PERSON_GROUP",
            joinColumns = {@JoinColumn(name = "person_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")})
    private final List<Group> groups = new ArrayList<>();
    @NotNull
    @Size(min = 2, max = 255)
    @Column(nullable = false)
    private String firstName;
    @NotNull
    @Size(min = 2, max = 255)
    @Column(nullable = false)
    private String lastName;
    private String patronymic;
    @NotNull
    @Pattern(regexp = "^(?:[a-zA-Z0-9_'^&/+-])+(?:\\.(?:[a-zA-Z0-9_'^&/+-])+)" +
            "*@(?:(?:\\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\\.)" +
            "{3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\]?)|(?:[a-zA-Z0-9-]+\\.)" +
            "+(?:[a-zA-Z]){2,}\\.?)$", message = "incorrect email")
    @Column(nullable = false, unique = true)
    private String email;
    @XmlJavaTypeAdapter(value = JaxbLocalDateAdapter.class)
    @NotNull
    @Column(nullable = false)
    private LocalDate birthday;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Sex sex;
    @XmlJavaTypeAdapter(value = JaxbNewLineTextAdapter.class)
    @Column(length = 65535, columnDefinition = "TEXT")
    private String info;
    @Column(name = "avatar_id")
    private String avatarId;
}
