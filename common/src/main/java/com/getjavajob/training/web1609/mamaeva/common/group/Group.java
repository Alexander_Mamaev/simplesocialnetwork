package com.getjavajob.training.web1609.mamaeva.common.group;

import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * The groups unite people by his interests, career etc.
 */
@Entity
@Table(name = "GROUPS")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode(of = {"name"}, callSuper = false)
@ToString(of = "name")
public class Group extends EntityId implements Serializable {
    private static final long serialVersionUID = 1L;
    @Singular
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "PERSON_GROUP",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "person_id")})
    private final Set<Person> subscribers = new HashSet<>();
    @Singular
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "GROUP_ADMINS",
            joinColumns = {@JoinColumn(name = "group_id")},
            inverseJoinColumns = {@JoinColumn(name = "person_id")})
    private final Set<Person> admins = new HashSet<>();
    @Column(nullable = false, unique = true)
    private String name;
    private String description;
    @Column(nullable = false)
    private LocalDate creationDate;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "owner_id")
    private Person owner;
}
