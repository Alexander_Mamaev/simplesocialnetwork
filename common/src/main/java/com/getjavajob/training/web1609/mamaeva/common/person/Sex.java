package com.getjavajob.training.web1609.mamaeva.common.person;

/**
 * Represents the sex of people
 */
public enum Sex {
    M, F
}
