package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ScriptOperations;
import org.springframework.data.mongodb.core.script.ExecutableMongoScript;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@ContextConfiguration(classes = {AccountDao.class})
public class AccountDaoTest extends MongoTest {
    @Autowired
    private AccountDao accountDao;

    /**
     * Run mongo script in Spring
     * https://docs.spring.io/spring-data/mongodb/docs/current/reference/html/#mongo.server-side-scripts
     */
    @Before
    public void before() throws IOException {
        ScriptOperations scriptOps = template.scriptOps();
        String script = loadScript("prepare_mongo.js");
        ExecutableMongoScript echoScript = new ExecutableMongoScript(script);
        scriptOps.execute(echoScript, "directly execute script");
    }

    @After
    public void after() {
        mongo.dropDatabase("mongo_test_db");
    }

    @Test
    public void findAccountByUsername() throws Exception {
        Account account1 = Account.builder()
                .username("1@mail.ru")
                .build();
        Account account2 = Account.builder()
                .username("2@mail.ru")
                .build();
        Account account3 = Account.builder()
                .username("3@mail.ru")
                .build();

        Account actual1 = accountDao.findAccountByUsername("1@mail.ru").orElse(null);
        assertThat(actual1, equalTo(account1));
        Account actual2 = accountDao.findAccountByUsername("2@mail.ru").orElse(null);
        assertThat(actual2, equalTo(account2));
        Account actual3 = accountDao.findAccountByUsername("3@mail.ru").orElse(null);
        assertThat(actual3, equalTo(account3));
    }

    @Test
    public void notFoundAccountByUsername() {
        Account actual = accountDao.findAccountByUsername("5@mail.ru").orElse(null);
        assertThat(actual, equalTo(null));
    }

    @Test
    public void saveAccount() {
        Account expected = Account.builder()
                .username("4@mail.ru")
                .build();

        accountDao.save(expected);
        Account actual = accountDao.findAccountByUsername("4@mail.ru").orElse(null);
        assertThat(actual, equalTo(expected));
    }

    @Test
    public void deleteAccount() throws Exception {
        Account account = accountDao.findAccountByUsername("1@mail.ru")
                .orElseThrow(() -> new Exception("Account with username 1@mail.ru must be into the database"));
        accountDao.delete(account);
        Account actual = accountDao.findAccountByUsername("1@mail.ru").orElse(null);
        assertThat(actual, equalTo(null));
    }

    @Test
    public void updateAccount() throws Exception {
        Account account = accountDao.findAccountByUsername("1@mail.ru")
                .orElseThrow(() -> new Exception("Account with username 1@mail.ru must be into the database"));
        account.setUsername("new@mail.ru");
        accountDao.save(account);
        Account actual = accountDao.findAccountByUsername("1@mail.ru").orElse(null);
        assertThat(actual, equalTo(null));
        Account actual2 = accountDao.findAccountByUsername("new@mail.ru").orElse(null);
        assertThat(actual2, equalTo(account));
    }
}