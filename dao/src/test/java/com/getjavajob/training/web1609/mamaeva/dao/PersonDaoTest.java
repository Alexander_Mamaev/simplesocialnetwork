package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Sex;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.validation.ValidationException;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;

@ContextConfiguration(classes = {PersonDao.class})
public class PersonDaoTest extends H2Test {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PersonDao personDao;

    @Test
    public void findOneTest() {
        Person person = Person.builder()
                .email("1@mail.ru")
                .build();
        assertThat(person, equalTo(personDao.findOne(1L)));
    }

    @Test
    public void findPersonByEmailTest() {
        Person person = Person.builder()
                .email("1@mail.ru")
                .firstName("firstName1")
                .patronymic("patronymic1")
                .birthday(LocalDate.of(2001, 1, 1))
                .build();

        Person actual = personDao.findPersonByEmail("1@mail.ru").orElse(null);
        if (actual == null) {
            Assert.fail();
        }
        assertThat(actual, equalTo(person));
        assertThat(actual.getFirstName(), equalTo(person.getFirstName()));
        assertThat(actual.getPatronymic(), equalTo(person.getPatronymic()));
        assertThat(actual.getBirthday(), equalTo(person.getBirthday()));

        Person nonexistent = personDao.findPersonByEmail("xxx").orElse(null);
        assertThat(nonexistent, equalTo(null));
    }

    @Test(expected = ValidationException.class)
    public void validationExceptionTest() {
        Person person = Person.builder()
                .email("newEmail")
                .build();
        personDao.save(person);
    }

    @Test
    public void saveTest() {
        Person person = Person.builder()
                .email("newEmail@mail.ru")
                .firstName("newFirst")
                .lastName("newLast")
                .patronymic("newPatronymic")
                .birthday(LocalDate.of(2001, 1, 1))
                .info("newInfo")
                .sex(Sex.F)
                .avatarId("123")
                .build();

        personDao.save(person);
        em.flush();

        Person actual = personDao.findPersonByEmail("newEmail@mail.ru").orElse(null);
        if (actual == null) {
            Assert.fail();
        }
        assertThat(actual.getFirstName(), equalTo(person.getFirstName()));
        assertThat(actual.getBirthday(), equalTo(person.getBirthday()));
    }

    @Test
    public void deleteTest() {
        Person person = personDao.findPersonByEmail("6@mail.ru").orElse(null);
        personDao.delete(person);
        Person actual = personDao.findPersonByEmail("6@mail.ru").orElse(null);
        assertThat(actual, equalTo(null));
    }

    @Test(expected = PersistenceException.class)
    public void deleteTestWithException() {
        Person person = personDao.findPersonByEmail("1@mail.ru").orElse(null);
        personDao.delete(person);
        em.flush();
    }

    @Test
    public void updateTest() {
        Person person = personDao.findPersonByEmail("1@mail.ru").orElse(null);
        assert person != null;
        person.setFirstName("newFirstName");
        personDao.save(person);
        em.flush();

        Person actual = personDao.findPersonByEmail("1@mail.ru").orElse(null);
        if (actual == null) {
            Assert.fail();
        }
        assertThat(actual.getFirstName(), equalTo(person.getFirstName()));
        assertThat(actual.getBirthday(), equalTo(person.getBirthday()));
    }

    @Test
    public void searchBarTest() {
        assertThat(personDao.searchBar("f", 3), hasSize(equalTo(3)));
        assertThat(personDao.searchBar("f"), hasSize(equalTo(9)));
    }

    @Test
    public void getFriendsTest() {
        Person person = personDao.findOne(1L);
        assertThat(person.getFriends(), hasSize(equalTo(3)));
    }

    @Test
    public void addFriendTest() {
        personDao.addFriend(1L, 5L);
        Person person = personDao.findOne(1L);
        assertThat(person.getFriends(), hasSize(equalTo(4)));
    }

    @Test
    public void findAllFriendsIdTest() {
        assertThat(personDao.findAllFriendsId(1L), hasSize(3));
    }

    @Test
    public void outcomeFriendRequestTest() {
        Person person = personDao.findOne(1L);
        assertThat(person.getOutcomeFriendRequest(), hasSize(3));
    }

    @Test
    public void incomeFriendRequestTest() {
        Person person = personDao.findOne(5L);
        assertThat(person.getIncomeFriendRequest(), hasSize(2));
    }

    @Test
    public void findAllOutcomeFriendRequestTest() {
        assertThat(personDao.findAllOutcomeFriendRequest(1L), hasSize(3));
    }

    @Test
    public void addFriendRequestTest() {
        assertThat(personDao.findAllOutcomeFriendRequest(1L), hasSize(3));
        personDao.addFriendRequest(1L, 8L);
        assertThat(personDao.findAllOutcomeFriendRequest(1L), hasSize(4));
    }

    @Test
    public void deleteFriendRequestsTest() {
        assertThat(personDao.findAllOutcomeFriendRequest(1L), hasSize(3));
        assertThat(personDao.findAllOutcomeFriendRequest(6L), hasSize(1));
        personDao.deleteFriendRequests(1L, 6L);
        personDao.deleteFriendRequests(11L, 66L);
        assertThat(personDao.findAllOutcomeFriendRequest(1L), hasSize(2));
        assertThat(personDao.findAllOutcomeFriendRequest(6L), hasSize(1));
    }

    @Test
    public void deleteFriendsTest() {
        Query query = em.createNativeQuery("SELECT person_id, friend_id FROM PERSON_FRIEND WHERE person_id = 1");
        List list = query.getResultList();
        assertThat(list.size(), equalTo(3));

        query = em.createNativeQuery("SELECT person_id, friend_id FROM PERSON_FRIEND WHERE person_id = 2");
        list = query.getResultList();
        assertThat(list.size(), equalTo(2));

        personDao.deleteFriends(1L, 2L);

        query = em.createNativeQuery("SELECT person_id, friend_id FROM PERSON_FRIEND WHERE person_id = 1");
        list = query.getResultList();
        assertThat(list.size(), equalTo(2));

        query = em.createNativeQuery("SELECT person_id, friend_id FROM PERSON_FRIEND WHERE person_id = 2");
        list = query.getResultList();
        assertThat(list.size(), equalTo(1));
    }

    @Test
    public void findByOrderByLastNameTest() {
        assertThat(personDao.findByOrderByLastName(new PageRequest(1, 2)),
                contains(
                        Person.builder().email("3@mail.ru").build(),
                        Person.builder().email("4@mail.ru").build())
        );
    }

    @Test
    public void findByLastNameStartingWithOrFirstNameStartingWithOrderByLastNameTest() {
        assertThat(personDao.findByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCaseOrderByLastName("lAs", "laS", new PageRequest(1, 2)),
                contains(
                        Person.builder().email("3@mail.ru").build(),
                        Person.builder().email("4@mail.ru").build()));
    }

    @Test
    public void countPersonByLastNameStartingWithOrFirstNameStartingWithTest() {
        assertThat(personDao.countPersonByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCase("lAs", "fiR"), equalTo(9));
    }
}