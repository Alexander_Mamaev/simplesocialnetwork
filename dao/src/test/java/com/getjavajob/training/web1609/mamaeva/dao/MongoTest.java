package com.getjavajob.training.web1609.mamaeva.dao;

import com.mongodb.Mongo;
import org.apache.commons.io.IOUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;

/**
 * Attention
 * Please check that firewall not block for mongo database creation
 */
@RunWith(SpringRunner.class)
@DataMongoTest // Worked with de.flapdoodle.embed.mongo version 1.50.0 and upper
@TestPropertySource("classpath:test_dao.properties")
public abstract class MongoTest {
    @Autowired
    protected Mongo mongo;
    @Autowired
    protected MongoTemplate template;

    /**
     * Read script from the File with requested path
     *
     * @throws IOException
     */
    public String loadScript(String resourceName) throws IOException {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(resourceName)) {
            return IOUtils.toString(is);
        }
    }
}
