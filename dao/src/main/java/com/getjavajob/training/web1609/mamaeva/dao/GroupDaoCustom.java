package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface GroupDaoCustom {
    List<Group> searchBar(@NotNull String query, @NotNull Integer limit);
}
