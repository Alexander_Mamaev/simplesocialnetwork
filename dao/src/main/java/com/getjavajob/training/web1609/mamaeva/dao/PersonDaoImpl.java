package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class PersonDaoImpl implements PersonDaoCustom {
    @Autowired
    private EntityManager em;

    @Override
    public List<Person> searchBar(@NotNull String query, @NotNull Integer limit) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Person> cq = cb.createQuery(Person.class);
        Root<Person> accountRoot = cq.from((Person.class));
        cq.select(accountRoot);
        Predicate firstName = cb.like(cb.lower(accountRoot.get("firstName")), query.toLowerCase() + "%");
        Predicate lastName = cb.like(cb.lower(accountRoot.get("lastName")), query.toLowerCase() + "%");
        Predicate p = cb.or(firstName, lastName);
        cq.where(p);
        return em.createQuery(cq)
                .setMaxResults(limit)
                .getResultList();
    }
}
