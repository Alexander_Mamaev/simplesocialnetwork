package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.AddressSimple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;

@Repository
public interface AddressSimpleDao extends JpaRepository<AddressSimple, Long> {
    AddressSimple findAddressSimpleByCountryAndRegionAndCityAndStreetAndHouseAndApartment(
            @NotNull String country,
            @NotNull String region,
            @NotNull String city,
            @NotNull String street,
            @NotNull String house,
            @NotNull String apartment
    );
}
