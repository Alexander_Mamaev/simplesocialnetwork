package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.Address;
import com.getjavajob.training.web1609.mamaeva.common.person.AddressSimple;
import com.getjavajob.training.web1609.mamaeva.common.person.PersonAddressKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface AddressDao extends JpaRepository<Address, PersonAddressKey> {
    List<Address> findAddressesByPersonId(@NotNull Long id);

    long countAddressByAddressSimple(@NotNull AddressSimple addressSimple);
}
