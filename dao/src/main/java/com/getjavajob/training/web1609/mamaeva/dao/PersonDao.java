package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface PersonDao extends JpaRepository<Person, Long>, PersonDaoCustom {
    Optional<Person> findPersonByEmail(@NotNull String email);

    List<Person> findByOrderByLastName(@NotNull Pageable p);

    List<Person> findByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCaseOrderByLastName(
            @NotNull String lastName, @NotNull String firstName, @NotNull Pageable p);

    Integer countPersonByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCase(
            @NotNull String lastName, @NotNull String firstName);

    @Query("SELECT p FROM Person p WHERE LOWER(p.firstName) LIKE LOWER(CONCAT(:query,'%')) OR LOWER(p.lastName) LIKE LOWER(CONCAT(:query, '%'))")
    List<Person> searchBar(@Param("query") @NotNull String query);

    @Modifying
    @Query(value = "INSERT INTO PERSON_FRIEND (person_id, friend_id) VALUES (:personId, :friendId)", nativeQuery = true)
    void addFriend(@Param("personId") @NotNull Long personId, @Param("friendId") @NotNull Long friendId);

    @Query(value = "SELECT friend_id FROM PERSON_FRIEND WHERE person_id = :personId", nativeQuery = true)
    List<BigInteger> findAllFriendsId(@Param("personId") @NotNull Long personId);

    @Query(value = "SELECT income_id FROM FRIEND_REQUEST WHERE outcome_id = :personId", nativeQuery = true)
    List<BigInteger> findAllOutcomeFriendRequest(@Param("personId") @NotNull Long personId);

    @Modifying
    @Query(value = "INSERT INTO FRIEND_REQUEST (outcome_id, income_id) VALUES (:outcomeId, :incomeId)", nativeQuery = true)
    void addFriendRequest(@Param("outcomeId") @NotNull Long outcomeId, @Param("incomeId") @NotNull Long incomeId);

    @Modifying
    @Query(value = "DELETE FROM FRIEND_REQUEST WHERE (outcome_id = :outcomeId AND income_id = :incomeId) OR (outcome_id = :incomeId AND income_id = :outcomeId)", nativeQuery = true)
    void deleteFriendRequests(@Param("outcomeId") @NotNull Long outcomeId, @Param("incomeId") @NotNull Long incomeId);

    @Modifying
    @Query(value = "DELETE FROM PERSON_FRIEND WHERE (person_id = :id1 AND friend_id = :id2) OR (person_id = :id2 AND friend_id = :id1)", nativeQuery = true)
    void deleteFriends(@Param("id1") @NotNull Long id1, @Param("id2") @NotNull Long id2);
}
