package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface GroupDao extends JpaRepository<Group, Long>, GroupDaoCustom {
    @Query("SELECT g FROM Group g WHERE LOWER(g.name) LIKE LOWER(CONCAT(:query,'%'))")
    List<Group> searchBar(@Param("query") @NotNull String query);
}
