package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public class GroupDaoImpl implements GroupDaoCustom {
    @Autowired
    private EntityManager em;

    @Override
    public List<Group> searchBar(@NotNull String query, @NotNull Integer limit) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Group> cq = cb.createQuery(Group.class);
        Root<Group> accountRoot = cq.from((Group.class));
        cq.select(accountRoot);
        Predicate name = cb.like(cb.lower(accountRoot.get("name")), query.toLowerCase() + "%");
        cq.where(name);
        return em.createQuery(cq)
                .setMaxResults(limit)
                .getResultList();
    }
}
