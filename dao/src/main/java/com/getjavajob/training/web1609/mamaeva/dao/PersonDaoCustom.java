package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;

import javax.validation.constraints.NotNull;
import java.util.List;

interface PersonDaoCustom {
    List<Person> searchBar(@NotNull String query, @NotNull Integer limit);
}
