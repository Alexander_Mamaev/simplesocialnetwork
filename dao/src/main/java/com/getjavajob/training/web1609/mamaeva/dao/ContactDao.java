package com.getjavajob.training.web1609.mamaeva.dao;

import com.getjavajob.training.web1609.mamaeva.common.person.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface ContactDao extends JpaRepository<Contact, Long> {
    List<Contact> findContactsByPersonId(@NotNull Long id);
}
