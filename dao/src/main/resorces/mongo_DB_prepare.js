// db = db.getSiblingDB('gjj_web_project');
db.accounts.save({
    "username": "1@mail.ru",
    "password": "$2a$11$xvmpheKErjLfEpabLWzJOuBbR7SgjBo84l80/r/VgWQ8DWjSiGt6C",
    "authorities": ["ROLE_ADMIN"],
    "accountNonExpired": true,
    "accountNonLocked": true,
    "credentialsNonExpired": true,
    "enabled": true
});
db.accounts.save({
    "username": "2@mail.ru",
    "password": "$2a$11$ywgUxEIFQzj9yWgGAAPRSuFwPP.vw2Vyy60IV6NCsT/KrM6QxTCZm",
    "authorities": ["ROLE_USER"],
    "accountNonExpired": true,
    "accountNonLocked": true,
    "credentialsNonExpired": true,
    "enabled": true
});
db.accounts.save({
    "username": "3@mail.ru",
    "password": "$2a$11$OsIIkLJuk8vcxXQzwLSl3ervz1eb4bxQ4nxrUtNvZ8WltcQsQWMP2",
    "authorities": ["ROLE_ANONYMOUS"],
    "accountNonExpired": true,
    "accountNonLocked": true,
    "credentialsNonExpired": true,
    "enabled": true
});