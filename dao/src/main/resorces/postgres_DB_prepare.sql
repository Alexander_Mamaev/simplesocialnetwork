DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA IF NOT EXISTS public;
SET SCHEMA 'public';

CREATE TABLE PERSONS (
  id         BIGSERIAL    NOT NULL,
  first_name VARCHAR(255) NOT NULL,
  last_name  VARCHAR(255) NOT NULL,
  patronymic VARCHAR(255),
  email      VARCHAR(255) NOT NULL UNIQUE,
  birthday   DATE,
  sex        VARCHAR(1),
  info       TEXT,
  avatar_id  VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE CONTACTS (
  id        BIGSERIAL    NOT NULL,
  value     VARCHAR(255) NOT NULL,
  type      VARCHAR(255) NOT NULL,
  person_id BIGINT       NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (person_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE
);

CREATE TABLE PHONES (
  id          BIGSERIAL    NOT NULL,
  number      VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL,
  person_id   BIGINT       NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (number, person_id),
  FOREIGN KEY (person_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE
);

CREATE TABLE SIMPLE_ADDRESSES (
  id        BIGSERIAL NOT NULL,
  country   VARCHAR(255),
  region    VARCHAR(255),
  city      VARCHAR(255),
  street    VARCHAR(255),
  house     VARCHAR(255),
  apartment VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE PERSON_ADDRESS (
  person_id         BIGINT NOT NULL,
  simple_address_id BIGINT NOT NULL,
  description       VARCHAR(255),
  PRIMARY KEY (person_id, simple_address_id),
  FOREIGN KEY (person_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE,
  FOREIGN KEY (simple_address_id) REFERENCES SIMPLE_ADDRESSES (id)
  ON DELETE CASCADE
);

CREATE TABLE PERSON_FRIEND (
  person_id BIGINT NOT NULL,
  friend_id BIGINT NOT NULL,
  PRIMARY KEY (person_id, friend_id),
  FOREIGN KEY (person_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE,
  FOREIGN KEY (friend_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE
);

CREATE TABLE FRIEND_REQUEST (
  outcome_id BIGINT NOT NULL,
  income_id  BIGINT NOT NULL,
  PRIMARY KEY (outcome_id, income_id),
  FOREIGN KEY (outcome_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE,
  FOREIGN KEY (income_id) REFERENCES PERSONS (id)
  ON DELETE CASCADE
);

CREATE TABLE GROUPS (
  id            BIGSERIAL    NOT NULL,
  name          VARCHAR(255) NOT NULL UNIQUE,
  description   VARCHAR(255),
  owner_id      BIGINT       NOT NULL,
  creation_date DATE         NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (owner_id) REFERENCES PERSONS (id)
);

CREATE TABLE PERSON_GROUP (
  person_id BIGINT NOT NULL,
  group_id  BIGINT NOT NULL,
  PRIMARY KEY (person_id, group_id),
  FOREIGN KEY (person_id) REFERENCES PERSONS (id),
  FOREIGN KEY (group_id) REFERENCES GROUPS (id)
);

CREATE TABLE GROUP_ADMINS (
  group_id  BIGINT NOT NULL,
  person_id BIGINT NOT NULL,
  PRIMARY KEY (group_id, person_id),
  FOREIGN KEY (group_id) REFERENCES GROUPS (id),
  FOREIGN KEY (person_id) REFERENCES PERSONS (id)
);

INSERT INTO PERSONS (first_name, last_name, patronymic, email, birthday, sex, info, avatar_id) VALUES
  ('firstName1', 'lastName1', 'patronymic1', '1@mail.ru', '2001-01-01', 'M', 'info1', '599c6a4bae3ddf1e0cc492ae'),
  ('firstName2', 'lastName2', 'patronymic2', '2@mail.ru', '2002-02-02', 'F', 'info2', ''),
  ('firstName3', 'lastName3', 'patronymic3', '3@mail.ru', '2003-03-03', 'M', 'info3', ''),
  ('firstName4', 'lastName4', 'patronymic4', '4@mail.ru', '2004-04-04', 'F', 'info4', ''),
  ('firstName5', 'lastName5', 'patronymic5', '5@mail.ru', '2005-05-05', 'M', 'info5', ''),
  ('firstName6', 'lastName6', 'patronymic6', '6@mail.ru', '2006-06-06', 'M', 'info6', ''),
  ('firstName7', 'lastName7', 'patronymic7', '7@mail.ru', '2007-07-07', 'M', 'info7', ''),
  ('firstName8', 'lastName8', 'patronymic8', '8@mail.ru', '2008-08-08', 'M', 'info8', ''),
  ('firstName9', 'lastName9', 'patronymic9', '9@mail.ru', '2009-09-09', 'M', 'info9', '');

INSERT INTO CONTACTS (value, type, person_id) VALUES
  ('dop1@mail.ru', 'email', 1),
  ('skype1', 'skype', 1),
  ('icq1', 'icq', 1),
  ('telegram1', 'telegram', 1),
  ('viber1', 'viber', 1),
  ('watsapp1', 'watsapp', 1);

INSERT INTO CONTACTS (value, type, person_id) VALUES
  ('dop2@mail.ru', 'email', 2),
  ('dop3@mail.ru', 'email', 2),
  ('skype2', 'skype', 2),
  ('icq2', 'icq', 2),
  ('telegram2', 'telegram', 2),
  ('viber2', 'viber', 2),
  ('watsapp2', 'watsapp', 2);

INSERT INTO PHONES (number, description, person_id) VALUES
  ('+71111111111', 'Personal', 1),
  ('+61111111111', 'Work', 1),
  ('+51111111111', 'Home', 1),
  ('+41111111111', 'Other', 1),
  ('+72222222222', 'Work', 2),
  ('+62222222222', 'Additional', 2),
  ('+73333333333', 'Home', 3),
  ('+74444444444', 'Work', 4),
  ('+75555555555', 'Other', 5),
  ('+76666666666', 'Additional', 6),
  ('+77777777777', 'Home', 7),
  ('+78888888888', 'Work', 8),
  ('+79999999999', 'Other', 9);

INSERT INTO SIMPLE_ADDRESSES (country, region, city, street, house, apartment) VALUES
  ('country1', 'region1', 'city1', 'street1', 'house1', 'apartment1'),
  ('country2', 'region2', 'city2', 'street2', 'house2', 'apartment2'),
  ('country3', 'region3', 'city3', 'street3', 'house3', 'apartment3'),
  ('country4', 'region4', 'city4', 'street4', 'house4', 'apartment4'),
  ('country5', 'region5', 'city5', 'street5', 'house5', 'apartment5'),
  ('country6', 'region6', 'city6', 'street6', 'house6', 'apartment6'),
  ('country7', 'region7', 'city7', 'street7', 'house7', 'apartment7'),
  ('country8', 'region8', 'city8', 'street8', 'house8', 'apartment8'),
  ('country9', 'region9', 'city9', 'street9', 'house9', 'apartment9'),
  ('country10', 'region10', 'city10', 'street10', 'house10', 'apartment10'),
  ('country11', 'region11', 'city11', 'street11', 'house11', 'apartment11'),
  ('country12', 'region12', 'city12', 'street12', 'house12', 'apartment12');


INSERT INTO PERSON_ADDRESS (person_id, simple_address_id, description) VALUES
  (1, 1, 'work'),
  (1, 2, 'home'),
  (1, 3, 'other'),
  (2, 4, 'home'),
  (2, 1, 'work'),
  (3, 5, 'home'),
  (3, 1, 'work'),
  (4, 6, 'home'),
  (4, 1, 'work'),
  (5, 7, 'home'),
  (5, 1, 'work'),
  (6, 8, 'home'),
  (7, 9, 'home'),
  (8, 10, 'home'),
  (9, 11, 'home');

INSERT INTO PERSON_FRIEND (person_id, friend_id) VALUES
  (1, 2),
  (2, 1),
  (1, 3),
  (3, 1),
  (3, 2),
  (2, 3),
  (5, 6),
  (6, 5),
  (5, 8),
  (8, 5);

INSERT INTO FRIEND_REQUEST (outcome_id, income_id) VALUES
  (1, 4),
  (1, 5),
  (1, 6),
  (2, 5),
  (2, 6),
  (2, 7);

INSERT INTO GROUPS (name, description, owner_id, creation_date) VALUES
  ('group1', 'description1', 1, '2001-01-01'),
  ('group2', 'description2', 1, '2001-01-01'),
  ('group3', 'description3', 1, '2001-01-01'),
  ('group4', 'description4', 2, '2002-02-02'),
  ('group5', 'description5', 3, '2003-03-03');

INSERT INTO PERSON_GROUP (person_id, group_id) VALUES
  (1, 2),
  (1, 3),
  (1, 4),
  (2, 4),
  (2, 5),
  (4, 2),
  (5, 3),
  (6, 1),
  (7, 2);

INSERT INTO GROUP_ADMINS (group_id, person_id) VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (4, 4),
  (5, 5);
