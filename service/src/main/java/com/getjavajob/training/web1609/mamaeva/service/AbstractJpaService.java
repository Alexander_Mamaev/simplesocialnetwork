package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Transactional(readOnly = true)
public abstract class AbstractJpaService<E extends EntityId, ID extends Serializable> extends AbstractService<E, ID> {
    public AbstractJpaService(JpaRepository<E, ID> dao) {
        super(dao);
    }

    private JpaRepository<E, ID> dao() {
        return (JpaRepository<E, ID>) dao;
    }

    /**
     * Save and then flush entity
     *
     * @return saved Entity
     */
    @Transactional
    public E saveAndFlush(@NotNull E entity) {
        return dao().saveAndFlush(entity);
    }
}
