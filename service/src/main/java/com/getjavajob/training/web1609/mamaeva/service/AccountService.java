package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.dao.AccountDao;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@Slf4j
@Service
@Transactional(readOnly = true)
public class AccountService extends AbstractMongoService<Account, ObjectId> implements UserDetailsService {

    public AccountService(MongoRepository<Account, ObjectId> dao) {
        super(dao);
    }

    private AccountDao dao() {
        return (AccountDao) dao;
    }

    /**
     * Find Account by username aka email.
     * That method uses by the Spring Security for user authentication
     */
    @Override
    public UserDetails loadUserByUsername(@NotNull String username) throws UsernameNotFoundException {
        UserDetails userDetails = dao().findAccountByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Account" + username + " was not found."));
        log.debug("User " + userDetails.getUsername() + " successfully load from db with Role = " + userDetails.getAuthorities());
        return userDetails;
    }

    /**
     * @return Account or null if such username not exists in the database
     */
    public Account findAccountByUserName(@NotNull String username) {
        return dao().findAccountByUsername(username).orElse(null);
    }
}
