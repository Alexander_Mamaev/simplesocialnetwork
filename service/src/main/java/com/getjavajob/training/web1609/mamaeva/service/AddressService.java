package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.person.Address;
import com.getjavajob.training.web1609.mamaeva.common.person.AddressSimple;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.dao.AddressDao;
import com.getjavajob.training.web1609.mamaeva.dao.AddressSimpleDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@Service
@Transactional(readOnly = true)
public class AddressService {
    private final AddressDao addressDao;
    private final AddressSimpleDao addressSimpleDao;

    @Autowired
    public AddressService(AddressDao addressDao, AddressSimpleDao addressSimpleDao) {
        this.addressDao = addressDao;
        this.addressSimpleDao = addressSimpleDao;
    }

    /**
     * Find List of Addresses for person by person id
     */
    public List<Address> findAddressesByPersonId(@NotNull Long id) {
        return addressDao.findAddressesByPersonId(id);
    }

    /**
     * Update information into the AddressSimple for person.
     * If no changes found into the address - does not require an update
     * If changes found - build and save new AddressSimple or use already saved in the database
     * If addressSimple have an id -> deleteAddress(...)
     */
    @Transactional
    public void updateAddress(@NotNull Person person, @NotNull AddressSimple addressSimple) {
        Address actualAddress;
        AddressSimple actualSimple =
                addressSimpleDao.findAddressSimpleByCountryAndRegionAndCityAndStreetAndHouseAndApartment(
                        addressSimple.getCountry(), addressSimple.getRegion(), addressSimple.getCity(),
                        addressSimple.getStreet(), addressSimple.getHouse(), addressSimple.getApartment());
        if (actualSimple != null) {
            if (actualSimple.getId().equals(addressSimple.getId())) {
                return; // does not require an update
            }
            actualAddress = Address.builder().person(person).addressSimple(actualSimple).build();
        } else {
            AddressSimple actual = addressSimple.copy();
            actual.setId(null);
            actualAddress = Address.builder().person(person).addressSimple(actual).build();
        }
        addressDao.save(actualAddress);
        if (addressSimple.getId() != null) {
            deleteAddress(person, addressSimple);
        }
    }

    /**
     * Delete AddressSimple from list of person contacts.
     * If that address used by other persons it is steel kept into the database
     */
    public void deleteAddress(@NotNull Person person, @NotNull AddressSimple addressSimple) {
        Address oldAddress = Address.builder().person(person).addressSimple(addressSimple).build();
        addressDao.delete(oldAddress);
        long tmp = addressDao.countAddressByAddressSimple(addressSimple);
        if (tmp == 0) {
            addressSimpleDao.delete(addressSimple);
        }
    }
}
