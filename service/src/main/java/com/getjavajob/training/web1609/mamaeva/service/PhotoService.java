package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.photos.Photo;
import com.getjavajob.training.web1609.mamaeva.dao.PhotoDao;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
@Service
@Transactional(readOnly = true)
public class PhotoService extends AbstractMongoService<Photo, ObjectId> {
    public PhotoService(MongoRepository<Photo, ObjectId> dao) {
        super(dao);
    }

    private PhotoDao dao() {
        return (PhotoDao) dao;
    }

    /**
     * Save byte[] as account avatar
     */
    @Transactional
    public Photo savePhoto(@NotNull Account account, byte[] file) {
        Photo photo = Photo.builder()
                .name("photoName") // TODO: 04.10.2017 replace of the fixed parameters of name, description and folder Name
                .description("description")
                .folderName("avatar")
                .account(account)
                .uploadTime(LocalDateTime.now().atZone(ZoneId.systemDefault()).toEpochSecond())
                .photo(file)
                .build();
        dao().save(photo);
        return photo;
    }

    /**
     * @return Photo from mongoDB searched by id
     */
    public Photo findPhotoById(ObjectId id) {
        return dao().findPhotoById(id);
    }
}
