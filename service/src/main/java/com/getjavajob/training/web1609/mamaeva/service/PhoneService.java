package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Phone;
import com.getjavajob.training.web1609.mamaeva.dao.PhoneDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class PhoneService extends AbstractJpaService<Phone, Long> {
    @PersistenceContext
    private EntityManager em;

    public PhoneService(JpaRepository<Phone, Long> dao) {
        super(dao);
    }

    private PhoneDao dao() {
        return (PhoneDao) dao;
    }

    /**
     * @return List of phones belonging to person with requested id
     */
    public List<Phone> findPhonesByPersonId(@NotNull Long id) {
        return dao().findPhonesByPersonId(id);
    }

    /**
     * Update List of phones for person
     */
    @Transactional
    public void updatePhones(@NotNull Person person, @NotNull List<Phone> phones) {
        List<Phone> actual = person.getPhones();
        List<Long> newPhoneIds = phones.stream().map(EntityId::getId).collect(Collectors.toList());
        actual.stream().filter(p -> !newPhoneIds.contains(p.getId())).forEach(dao()::delete);
        em.flush();
        dao().save(phones);
    }
}
