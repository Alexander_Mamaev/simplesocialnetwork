package com.getjavajob.training.web1609.mamaeva.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import com.getjavajob.training.web1609.mamaeva.common.person.Contact;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Phone;
import com.getjavajob.training.web1609.mamaeva.common.photos.Photo;
import com.getjavajob.training.web1609.mamaeva.dao.PersonDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.List;

@Slf4j
@Service
@Transactional(readOnly = true)
public class PersonService extends AbstractJpaService<Person, Long> {
    private final ContactService contactService;
    private final AddressService addressService;
    private final PhoneService phoneService;

    @Autowired
    public PersonService(JpaRepository<Person, Long> dao,
                         ContactService contactService,
                         AddressService addressService,
                         PhoneService phoneService) {
        super(dao);
        this.contactService = contactService;
        this.addressService = addressService;
        this.phoneService = phoneService;
    }

    private PersonDao dao() {
        return (PersonDao) dao;
    }

    /**
     * Search person into the database by email
     *
     * @return Person
     */
    public Person findPersonByEmail(@NotNull String email) {
        return dao().findPersonByEmail(email).orElse(null);
    }

    /**
     * Search All Persons by query string
     */
    public List<Person> searchBar(@NotNull String query) {
        return dao().searchBar(query);
    }

    /**
     * Search All Persons by query string and limit top 'limit' founded elements
     */
    public List<Person> searchBar(@NotNull String query, @NotNull Integer limit) {
        return dao().searchBar(query, limit);
    }

    /**
     * Set photo as person avatar (save avatar_id into the person table)
     */
    @Transactional
    public void setAvatar(@NotNull Person person, @NotNull Photo photo) {
        person.setAvatarId(photo.getId().toString());
        dao().save(person);
    }

    /**
     * Export person data to the xml format with jackson parser
     */
    public String parseToXml(@NotNull Person person) throws ServiceException {
        ObjectMapper mapper = new XmlMapper();
        mapper.setAnnotationIntrospector(new JaxbAnnotationIntrospector(mapper.getTypeFactory()));
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        String xml;
        try {
            xml = mapper.writeValueAsString(person);
        } catch (JsonProcessingException e) {
            log.warn("parseToXml error");
            throw new ServiceException("parseToXml error", e);
        }
        return xml;
    }

    /**
     * Export person data to the xml format with JAXB parser
     */
    public String jaxbParseToXml(@NotNull Person person) throws ServiceException {
        StringWriter sw = new StringWriter();
        try {
            JAXBContext context = JAXBContext.newInstance(Person.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(person, sw);
        } catch (JAXBException e) {
            log.warn("jaxbParseToXml error");
            throw new ServiceException("jaxbParseToXml error", e);
        }
        log.info("success xml parsing for account: {}", person);
        return sw.toString();
    }

    /**
     * Import person reconstructed from String that represents xml file
     *
     * @return Person with data that contains into the string (xml)
     */
    public Person jaxbParseFromXML(@NotNull String xml) throws ServiceException {
        Person person;
        try {
            JAXBContext jc = JAXBContext.newInstance(Person.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            StringReader reader = new StringReader(xml);
            person = (Person) unmarshaller.unmarshal(reader);
        } catch (JAXBException e) {
            throw new ServiceException("jaxbParseFromXML error", e);
        }
        log.info("success xml parsing from file for account: {}", person);
        return person;
    }

    /**
     * Update current person info, phones, contacts and addresses from updated Person object
     *
     * @param current - updatable Person
     * @param updated - Person object that contains new data
     * @return Person with updated fields
     */
    @Transactional
    public Person updatePerson(@NotNull Person current, @NotNull Person updated) throws ServiceException {
        if (!updated.getEmail().equals(current.getEmail())) {
            throw new ServiceException("Update person form XML: Its is forbidden to change email");
        }
        current.setFirstName(updated.getFirstName());
        current.setLastName(updated.getLastName());
        current.setPatronymic(updated.getPatronymic());
        current.setBirthday(updated.getBirthday());
        current.setSex(updated.getSex());
        current.setInfo(updated.getInfo());
        current.setAvatarId(updated.getAvatarId());

        save(current);

        List<Contact> updatedContacts = updated.getContacts();
        updatedContacts.forEach(c -> {
            c.setPerson(current);
            c.setId(null);
        });
        contactService.updateContacts(current, updatedContacts);

        List<Phone> updatedPhones = updated.getPhones();
        updatedPhones.forEach(p -> p.setPerson(current));
        phoneService.updatePhones(current, updatedPhones);

        updated.getAddresses().forEach(a ->
                addressService.updateAddress(current, a.getAddressSimple())
        );
        return current;
    }

    /**
     * Add friends relationship to the database
     */
    @Transactional
    public void addFriend(@NotNull Long personId, @NotNull Long friendId) {
        dao().addFriend(personId, friendId);
        dao().addFriend(friendId, personId);
        dao().deleteFriendRequests(personId, friendId);
    }

    /**
     * Add friend request
     *
     * @param outcomeId - person who initiate request
     * @param incomeId  - person who accepts the proposal or not
     */
    @Transactional
    public void addFriendRequest(@NotNull Long outcomeId, @NotNull Long incomeId) {
        dao().addFriendRequest(outcomeId, incomeId);
    }

    /**
     * Delete (decline) friend request
     */
    @Transactional
    public void deleteFriendRequests(@NotNull Long outcomeId, @NotNull Long incomeId) {
        dao().deleteFriendRequests(outcomeId, incomeId);
    }

    /**
     * Delete friend relationship between two persons with the corresponding identifier
     */
    @Transactional
    public void deleteFriends(@NotNull Long id1, @NotNull Long id2) {
        dao().deleteFriends(id1, id2);
    }

    public boolean checkFriend(@NotNull Long personId, @NotNull Long friendId) {
        List<BigInteger> friendsId = dao().findAllFriendsId(personId);
        List<BigInteger> outcomesId = dao().findAllOutcomeFriendRequest(personId);
        return friendsId.stream().map(BigInteger::longValue).anyMatch(p -> p.equals(friendId)) ||
                outcomesId.stream().map(BigInteger::longValue).anyMatch(p -> p.equals(friendId));
    }

    /**
     * Find all Persons and ordering them by last name
     */
    public List<Person> findByOrderByLastName(@NotNull Pageable p) {
        return dao().findByOrderByLastName(p);
    }

    /**
     * Find all persons who first name and last name start with 'firstName' and 'lastName' respectively. And limit selection according to 'p'
     */
    public List<Person> findWithQueryMatch(@NotNull String lastName, @NotNull String firstNme, @NotNull Pageable p) {
        return dao().findByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCaseOrderByLastName(lastName, firstNme, p);
    }

    /**
     * Count all persons who was selected with this request:
     * Find all persons who first name and last name start with 'firstName' and 'lastName' respectively.
     */
    public Integer countPersonWithQueryMatch(@NotNull String lastName, @NotNull String firstName) {
        return dao().countPersonByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCase(lastName, firstName);
    }

}
