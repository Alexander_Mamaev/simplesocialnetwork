package com.getjavajob.training.web1609.mamaeva.service;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Transactional(readOnly = true)
public abstract class AbstractService<E, ID extends Serializable> {
    protected PagingAndSortingRepository<E, ID> dao;

    public AbstractService(PagingAndSortingRepository<E, ID> dao) {
        this.dao = dao;
    }

    /**
     * Save Entity
     *
     * @return saved Entity
     */
    @Transactional
    public E save(@NotNull E entity) {
        return dao.save(entity);
    }

    /**
     * Saves all given entities.
     *
     * @return the saved entities
     */
    @Transactional
    public Iterable<E> save(@NotNull Iterable<E> entities) {
        return dao.save(entities);
    }

    /**
     * Find entity into the database by given id
     */
    public E findOne(@NotNull ID id) {
        return dao.findOne(id);
    }

    /**
     * Delete entity
     */
    @Transactional
    public void delete(@NotNull E entity) {
        dao.delete(entity);
    }

    /**
     * Delete all given entities.
     */
    @Transactional
    public void delete(@NotNull List<E> entities) {
        dao.delete(entities);
    }
}
