package com.getjavajob.training.web1609.mamaeva.service;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public abstract class AbstractMongoService<E, ID extends ObjectId> extends AbstractService<E, ID> {
    public AbstractMongoService(MongoRepository<E, ID> dao) {
        super(dao);
    }
}
