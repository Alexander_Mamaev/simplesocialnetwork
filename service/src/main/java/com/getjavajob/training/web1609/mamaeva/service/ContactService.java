package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.EntityId;
import com.getjavajob.training.web1609.mamaeva.common.person.Contact;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.dao.ContactDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class ContactService extends AbstractJpaService<Contact, Long> {
    public ContactService(JpaRepository<Contact, Long> dao) {
        super(dao);
    }

    private ContactDao dao() {
        return (ContactDao) dao;
    }

    /**
     * Find List of the contacts for person with requested id
     */
    public List<Contact> findContactsByPersonId(@NotNull Long id) {
        return dao().findContactsByPersonId(id);
    }

    /**
     * Update contacts of person
     */
    @Transactional
    public void updateContacts(@NotNull Person person, @NotNull List<Contact> contacts) {
        List<Contact> actual = person.getContacts();
        List<Long> newContactIds = contacts.stream().map(EntityId::getId).collect(Collectors.toList());
        actual.stream().filter(c -> !newContactIds.contains(c.getId())).forEach(dao()::delete);
        dao().save(contacts);
    }
}
