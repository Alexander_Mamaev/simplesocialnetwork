package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.dto.AccountPersonDTO;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@Slf4j
@Service
@Transactional(readOnly = true)
public class AccountPersonService {
    private final AccountService accountService;
    private final PersonService personService;

    @Autowired
    public AccountPersonService(AccountService accountService, PersonService personService) {
        this.accountService = accountService;
        this.personService = personService;
    }

    /**
     * Add Account and Person to the DB at one transaction
     */
    @Transactional
    public Account registerAccount(@NotNull AccountPersonDTO dto) {
        personService.save(dto.getPerson());
        return accountService.save(dto.getAccount());
    }

    /**
     * Update Account email with Person data at one transaction
     */
    @Transactional
    public void updateAccountEmail(@NotNull String email, @NotNull Account account, @NotNull Person person) {
        person.setEmail(email);
        personService.saveAndFlush(person);
        account.setUsername(email);
        accountService.save(account);
    }
}
