package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import com.getjavajob.training.web1609.mamaeva.dao.GroupDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class GroupService extends AbstractJpaService<Group, Long> {
    public GroupService(JpaRepository<Group, Long> dao) {
        super(dao);
    }

    private GroupDao dao() {
        return (GroupDao) dao;
    }

    /**
     * Search All Groups by query string
     */
    public List<Group> searchBar(@NotNull String query) {
        return dao().searchBar(query);
    }

    /**
     * Search All Groups by query string and limit top 'limit' results
     */
    public List<Group> searchBar(@NotNull String query, @NotNull Integer limit) {
        return dao().searchBar(query, limit);
    }
}
