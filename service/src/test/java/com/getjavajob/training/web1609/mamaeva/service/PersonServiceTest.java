package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import com.getjavajob.training.web1609.mamaeva.common.person.*;
import com.getjavajob.training.web1609.mamaeva.common.photos.Photo;
import com.getjavajob.training.web1609.mamaeva.dao.PersonDao;
import org.bson.types.ObjectId;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {
    private static Person person;
    @InjectMocks
    private PersonService personService;
    @Mock
    private PersonDao personDao;
    @Mock
    private ContactService contactService;
    @Mock
    private PhoneService phoneService;

    @BeforeClass
    public static void beforeClass() {
        person = Person.builder()
                .firstName("firstName")
                .lastName("lastName")
                .patronymic("patronymic")
                .email("email")
                .info("info")
                .sex(Sex.M)
                .birthday(LocalDate.of(2017, 10, 10))
                .avatarId("avatarId")
                .build();
        person.setId(1L);

        person.getPhones().add(Phone.builder().number("+71234567890").build());
        person.getPhones().add(Phone.builder().number("+81234567890").build());
        person.getAddresses().add(Address.builder()
                .addressSimple(AddressSimple.builder()
                        .country("Country")
                        .city("City")
                        .street("Street")
                        .house("house")
                        .apartment("apartment")
                        .build())
                .build());
        person.getFriends().add(Person.builder().firstName("firstName").build());
        person.getFriends().add(Person.builder().lastName("lastName").build());
        person.getGroups().add(Group.builder().name("Group").build());
        person.getGroups().add(Group.builder().name("newGroup").build());
    }

    @Test
    public void findPersonById() throws Exception {
        when(personDao.findOne(person.getId())).thenReturn(person);
        assertEquals(person, personService.findOne(person.getId()));
        verify(personDao, times(1)).findOne(person.getId());
    }

    @Test
    public void findPersonByEmail() throws Exception {
        when(personDao.findPersonByEmail(person.getEmail())).thenReturn(Optional.of(person));
        assertEquals(person, personService.findPersonByEmail(person.getEmail()));
        verify(personDao, times(1)).findPersonByEmail(person.getEmail());
    }

    @Test
    public void save() throws Exception {
        when(personDao.save(person)).thenReturn(person);
        assertEquals(person, personService.save(person));
        verify(personDao, times(1)).save(person);
    }

    @Test
    public void saveAndFlush() throws Exception {
        when(personDao.saveAndFlush(person)).thenReturn(person);
        assertEquals(person, personService.saveAndFlush(person));
        verify(personDao, times(1)).saveAndFlush(person);
    }

    @Test
    public void remove() throws Exception {
        personService.delete(person);
        verify(personDao, times(1)).delete(person);
    }

    @Test
    public void setAvatar() throws Exception {
        Person person = Person.builder().build();
        Photo photo = Photo.builder()
                .id(ObjectId.get())
                .build();
        person.setAvatarId(String.valueOf(photo.getId()));
        assertEquals(String.valueOf(photo.getId()), person.getAvatarId());
    }

    @Test
    public void parseToXml() throws Exception {
        String expected = fileToStringConverter("/jackson_xml.xml");
        assertEquals(
                personService.parseToXml(person).replace("\n", "").replace("\r", ""),
                expected.replace("\n", "").replace("\r", ""));
    }

    @Test
    public void jaxbParseToXml() throws Exception {
        String expected = fileToStringConverter("/jaxb_xml.xml");
        assertEquals(
                personService.jaxbParseToXml(person).replace("\n", "").replace("\r", ""),
                expected.replace("\n", "").replace("\r", ""));
    }

    @Test
    public void jaxbParseFromXML() throws Exception {
        String xml = fileToStringConverter("/jaxb_xml.xml");
        Person actual = personService.jaxbParseFromXML(xml);
        assertEquals(person, actual);
        assertEquals(person.getFirstName(), actual.getFirstName());
        assertEquals(person.getBirthday(), actual.getBirthday());
        assertThat(person.getPhones(), equalTo(actual.getPhones()));
    }

    @Test(expected = ServiceException.class)
    public void updatePersonException() throws Exception {
        Person current = Person.builder()
                .email("email")
                .firstName("firstName")
                .build();

        Person updated = Person.builder()
                .email("updated")
                .firstName("updated")
                .build();

        personService.updatePerson(current, updated);
    }

    @Test
    public void updatePerson() throws Exception {
        Person current = Person.builder()
                .email("email")
                .firstName("firstName")
                .build();

        Person updated = Person.builder()
                .email("email")
                .firstName("updated")
                .build();

        Person actual = personService.updatePerson(current, updated);
        assertEquals(actual, current);
        assertEquals(actual.getFirstName(), current.getFirstName());
        verify(contactService, times(1)).updateContacts(current, updated.getContacts());
        verify(phoneService, times(1)).updatePhones(current, updated.getPhones());
    }

    @Test
    public void checkFriendTest() {
        List<BigInteger> list = Arrays.asList(BigInteger.valueOf(2), BigInteger.valueOf(3), BigInteger.valueOf(4));
        List<BigInteger> outcomes = Arrays.asList(BigInteger.valueOf(4), BigInteger.valueOf(5), BigInteger.valueOf(6));
        when(personDao.findAllFriendsId(1L)).thenReturn(list);
        when(personDao.findAllOutcomeFriendRequest(1L)).thenReturn(outcomes);
        assertEquals(personService.checkFriend(1L, 3L), true);
        assertEquals(personService.checkFriend(1L, 5L), true);
        assertEquals(personService.checkFriend(1L, 8L), false);
        verify(personDao, times(3)).findAllFriendsId(1L);
        verify(personDao, times(3)).findAllOutcomeFriendRequest(1L);
    }

    @Test
    public void addFriendRequestTest() {
        personService.addFriendRequest(1L, 8L);
        verify(personDao, times(1)).addFriendRequest(1L, 8L);
    }

    @Test
    public void deleteFriendRequestTest() {
        personService.deleteFriendRequests(1L, 6L);
        verify(personDao, times(1)).deleteFriendRequests(1L, 6L);
    }

    @Test
    public void deleteFriendsTest() {
        personService.deleteFriends(1L, 2L);
        verify(personDao, times(1)).deleteFriends(1L, 2L);
    }

    @Test
    public void findByOrderByLastNameTest() {
        personService.findByOrderByLastName(new PageRequest(1, 2));
        verify(personDao, times(1)).findByOrderByLastName(new PageRequest(1, 2));
    }

    @Test
    public void findWithQueryMatchTest() {
        personService.findWithQueryMatch("las", "las", new PageRequest(1, 2));
        verify(personDao, times(1)).findByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCaseOrderByLastName("las", "las", new PageRequest(1, 2));
    }

    @Test
    public void countPersonWithQueryMatchTest() {
        personService.countPersonWithQueryMatch("las", "las");
        verify(personDao, times(1)).countPersonByLastNameStartingWithIgnoreCaseOrFirstNameStartingWithIgnoreCase("las", "las");
    }

    @SuppressWarnings("Duplicates")
    private String fileToStringConverter(String fileName) throws IOException {
        try (InputStream is = getClass().getResourceAsStream(fileName);
             ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF-8");
        } catch (IOException e) {
            throw new IOException(e.getMessage(), e);
        }
    }
}