package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.person.Address;
import com.getjavajob.training.web1609.mamaeva.common.person.AddressSimple;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.dao.AddressDao;
import com.getjavajob.training.web1609.mamaeva.dao.AddressSimpleDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class AddressServiceTest {
    private static Person person;
    private static AddressSimple addressSimple;
    @InjectMocks
    private AddressService addressService;
    @Mock
    private AddressDao addressDao;
    @Mock
    private AddressSimpleDao addressSimpleDao;

    @BeforeClass
    public static void beforeClass() {
        person = Person.builder()
                .email("email")
                .build();

        addressSimple = AddressSimple.builder()
                .country("country")
                .region("region")
                .city("city")
                .street("street")
                .house("house")
                .apartment("apartment")
                .build();
        addressSimple.setId(1L);
    }

    @Before
    public void before() {
        when(addressSimpleDao.findAddressSimpleByCountryAndRegionAndCityAndStreetAndHouseAndApartment(
                addressSimple.getCountry(),
                addressSimple.getRegion(),
                addressSimple.getCity(),
                addressSimple.getStreet(),
                addressSimple.getHouse(),
                addressSimple.getApartment()
        )).thenReturn(addressSimple);
    }

    @Test
    public void findAddressesByPersonIdTest() {
        List<Address> addresses = new ArrayList<>();
        addresses.add(Address.builder().build());
        addresses.add(Address.builder().build());
        when(addressDao.findAddressesByPersonId(1L)).thenReturn(addresses);
        assertThat(addressService.findAddressesByPersonId(1L), hasSize(2));
    }

    @Test
    public void updateAddressTestCase_1() throws Exception {
        addressService.updateAddress(person, addressSimple);
        Address address = Address.builder().person(person).addressSimple(addressSimple).build();
        verify(addressDao, times(0)).save(address);
    }

    @Test
    public void updateAddressTestCase_2() throws Exception {
        AddressSimple newAddress = AddressSimple.builder()
                .country("country")
                .region("region")
                .city("city")
                .street("street")
                .house("house")
                .apartment("apartment")
                .build();

        addressService.updateAddress(person, newAddress);

        Address address = Address.builder().person(person).addressSimple(newAddress).build();
        Address oldAddress = Address.builder().person(person).addressSimple(addressSimple).build();
        verify(addressDao, times(1)).save(address);
        verify(addressDao, times(0)).delete(oldAddress);
    }

    @Test
    public void updateAddressTestCase_3() throws Exception {
        AddressSimple newAddress = AddressSimple.builder()
                .country("country")
                .region("region")
                .city("city")
                .street("street")
                .house("house")
                .apartment("apartment")
                .build();
        newAddress.setId(2L);

        addressService.updateAddress(person, newAddress);

        Address address = Address.builder().person(person).addressSimple(newAddress).build();
        Address oldAddress = Address.builder().person(person).addressSimple(addressSimple).build();
        verify(addressDao, times(1)).save(address);
        verify(addressDao, times(1)).delete(oldAddress);
    }

    @Test
    public void deleteAddressTest() throws Exception {
        when(addressDao.countAddressByAddressSimple(addressSimple)).thenReturn(0L);
        addressService.deleteAddress(person, addressSimple);
        Address oldAddress = Address.builder().person(person).addressSimple(addressSimple).build();
        verify(addressDao, times(1)).delete(oldAddress);
        verify(addressSimpleDao, times(1)).delete(addressSimple);
    }
}