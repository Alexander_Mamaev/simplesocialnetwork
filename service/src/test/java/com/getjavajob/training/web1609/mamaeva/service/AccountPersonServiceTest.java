package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.dto.AccountPersonDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class AccountPersonServiceTest {
    private static AccountPersonDTO dto;
    @InjectMocks
    private AccountPersonService accountPersonService;
    @Mock
    private AccountService accountService;
    @Mock
    private PersonService personService;

    @BeforeClass
    public static void beforeClass() {
        dto = AccountPersonDTO.builder()
                .email("email")
                .build();
    }

    @Test
    public void registerAccount() throws Exception {
        accountPersonService.registerAccount(dto);
        verify(accountService, times(1)).save(dto.getAccount());
        verify(personService, times(1)).save(dto.getPerson());
    }

    @Test
    public void updateAccountEmail() throws Exception {
        accountPersonService.updateAccountEmail("email", dto.getAccount(), dto.getPerson());
        verify(accountService, times(1)).save(dto.getAccount());
        verify(personService, times(1)).saveAndFlush(dto.getPerson());
    }
}