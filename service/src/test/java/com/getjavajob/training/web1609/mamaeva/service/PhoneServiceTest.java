package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Phone;
import com.getjavajob.training.web1609.mamaeva.dao.PhoneDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class PhoneServiceTest {
    @InjectMocks
    private PhoneService phoneService;
    @Mock
    private PhoneDao phoneDao;

    @Before
    public void before() throws NoSuchFieldException, IllegalAccessException {
        Field field = phoneService.getClass().getDeclaredField("em");
        field.setAccessible(true);
        EntityManager em = mock(EntityManager.class);
        field.set(phoneService, em);
        doNothing().when(em).flush();
    }

    @Test
    public void findPhonesByPersonId() throws Exception {
        phoneService.findPhonesByPersonId(1L);
        verify(phoneDao, times(1)).findPhonesByPersonId(1L);
    }

    @Test
    public void updatePhones() throws Exception {
        Person person = Person.builder()
                .email("email")
                .build();
        Phone phone_1 = Phone.builder()
                .number("number_1")
                .build();
        phone_1.setId(1L);
        Phone phone_2 = Phone.builder()
                .number("number_2")
                .build();
        phone_2.setId(2L);
        List<Phone> phones = person.getPhones();
        phones.add(phone_1);
        phones.add(phone_2);

        List<Phone> phoneList = new ArrayList<>();
        Phone phone_3 = Phone.builder()
                .number("number_3")
                .build();
        phone_3.setId(3L);
        phoneList.add(phone_2);
        phoneList.add(phone_3);

        phoneService.updatePhones(person, phoneList);
        verify(phoneDao, times(1)).delete(phone_1);
        verify(phoneDao, times(1)).save(phoneList);
    }
}