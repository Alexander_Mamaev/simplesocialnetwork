package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.person.Contact;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.dao.ContactDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class ContactServiceTest {
    @InjectMocks
    private ContactService contactService;
    @Mock
    private ContactDao contactDao;

    @Test
    public void findContactsByPersonId() throws Exception {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(Contact.builder().build());
        contacts.add(Contact.builder().build());
        when(contactDao.findContactsByPersonId(0L)).thenReturn(contacts);

        assertThat(contactService.findContactsByPersonId(0L), equalTo(contacts));
        verify(contactDao).findContactsByPersonId(0L);
    }

    @Test
    public void updateContacts() throws Exception {
        Person person = Person.builder()
                .email("email")
                .build();
        List<Contact> contacts = person.getContacts();
        Contact contact_1 = Contact.builder()
                .value("value_1")
                .type("type_1")
                .build();
        contact_1.setId(1L);
        Contact contact_2 = Contact.builder()
                .value("value_2")
                .type("type_2")
                .build();
        contact_1.setId(2L);
        contacts.add(contact_1);
        contacts.add(contact_2);

        List<Contact> contactList = new ArrayList<>();
        Contact contact_3 = Contact.builder()
                .value("value_3")
                .type("type_3")
                .build();
        contact_3.setId(3L);
        contactList.add(contact_2);
        contactList.add(contact_3);

        contactService.updateContacts(person, contactList);
        verify(contactDao, times(1)).delete(contact_1);
        verify(contactDao, times(1)).save(contactList);
    }
}