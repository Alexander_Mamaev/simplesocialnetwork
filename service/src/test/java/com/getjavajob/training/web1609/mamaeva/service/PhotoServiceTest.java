package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.photos.Photo;
import com.getjavajob.training.web1609.mamaeva.dao.PhotoDao;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class PhotoServiceTest {
    @InjectMocks
    private PhotoService photoService;
    @Mock
    private PhotoDao photoDao;

    @Test
    public void savePhoto() throws Exception {
        Photo photo = Photo.builder()
                .photo(new byte[]{})
                .build();
        photoService.savePhoto(Account.builder().build(), photo.getPhoto());
        verify(photoDao, times(1)).save(photo);
    }

    @Test
    public void findPhotoById() throws Exception {
        Photo photo = Photo.builder().build();
        ObjectId objectId = ObjectId.get();
        when(photoService.findPhotoById(objectId)).thenReturn(photo);
        photoService.findPhotoById(objectId);
        verify(photoDao).findPhotoById(objectId);
    }
}