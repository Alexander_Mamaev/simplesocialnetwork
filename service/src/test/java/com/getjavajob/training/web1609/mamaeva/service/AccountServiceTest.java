package com.getjavajob.training.web1609.mamaeva.service;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.account.Role;
import com.getjavajob.training.web1609.mamaeva.dao.AccountDao;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    private static Account user;
    @InjectMocks
    private AccountService accountService;
    @Mock
    private AccountDao accountDao;

    @BeforeClass
    public static void beforeClass() {
        user = Account.builder()
                .id(ObjectId.get())
                .username("userName")
                .password("password")
                .authority(Role.ROLE_USER)
                .build();
    }

    @Test
    public void findAccountByUserNameTest() {
        when(accountDao.findAccountByUsername("userName")).thenReturn(Optional.of(user));
        assertEquals(user, accountService.findAccountByUserName(user.getUsername()));
        verify(accountDao, times(1)).findAccountByUsername(user.getUsername());
    }

    @Test
    public void loadUserByUsernameTest() {
        when(accountDao.findAccountByUsername("userName")).thenReturn(Optional.of(user));
        assertEquals(user, accountService.loadUserByUsername(user.getUsername()));
        verify(accountDao, times(1)).findAccountByUsername(user.getUsername());
    }

    @Test
    public void loadUserByUsernameTestException() {
        when(accountDao.findAccountByUsername("userName")).thenReturn(Optional.empty());
        try {
            accountService.loadUserByUsername(user.getUsername());
        } catch (Exception e) {
            assertEquals(e.getClass(), UsernameNotFoundException.class);
        }
        verify(accountDao, times(1)).findAccountByUsername(user.getUsername());
    }

    @Test
    public void saveTest() {
        when(accountDao.save(user)).thenReturn(user);
        assertEquals(user, accountService.save(user));
        verify(accountDao, times(1)).save(user);
    }

    @Test
    public void deleteTest() {
        accountService.delete(user);
        verify(accountDao, times(1)).delete(user);
    }
}