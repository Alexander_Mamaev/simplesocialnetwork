package com.getjavajob.training.web1609.mamaeva.web.api;

import com.getjavajob.training.web1609.mamaeva.common.dto.ContactDTO;
import com.getjavajob.training.web1609.mamaeva.common.dto.PhoneDTO;
import com.getjavajob.training.web1609.mamaeva.common.message.StatusMessage;
import com.getjavajob.training.web1609.mamaeva.common.person.*;
import com.getjavajob.training.web1609.mamaeva.service.*;
import com.getjavajob.training.web1609.mamaeva.web.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.*;
import javax.validation.constraints.NotNull;
import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.getjavajob.training.web1609.mamaeva.common.message.StatusMessage.*;

@Slf4j
@Controller
public class PersonApi {
    private final WebUtil webUtil;
    private final PersonService personService;
    private final PhoneService phoneService;
    private final AddressService addressService;
    private final ContactService contactService;

    @Autowired
    public PersonApi(WebUtil webUtil, PersonService personService, PhoneService phoneService, AddressService addressService, ContactService contactService) {
        this.webUtil = webUtil;
        this.personService = personService;
        this.phoneService = phoneService;
        this.addressService = addressService;
        this.contactService = contactService;
    }

    /**
     * Allow bind iso-date format to LocalDate
     */
    @InitBinder
    public void customizeBinding(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, "birthday", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(LocalDate.parse(text, DateTimeFormatter.ISO_DATE));
            }
        });
    }

    /**
     * @param id of the person
     * @return email of person
     */
    @ResponseBody
    @GetMapping(value = "/public/api/persons/{id}", produces = "text/plain;charset=utf-8")
    public String getPublicAccount(@PathVariable @NotNull Long id) {
        return personService.findOne(id).getEmail();
    }

    /**
     * @return authenticated Person
     * @throws ServletException
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personAuth", produces = "application/json;charset=UTF-8")
    public Person personAuth() throws ServletException {
        return personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
    }

    /**
     * @param email
     * @return Person with requested email
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personInfo", produces = "application/json;charset=UTF-8")
    public Person personInfo(@RequestParam @NotNull String email) {
        return personService.findPersonByEmail(email);
    }

    /**
     * @param id
     * @return Person with requested email
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personInfoById", produces = "application/json;charset=UTF-8")
    public Person personInfoById(@RequestParam @NotNull Long id) {
        return personService.findOne(id);
    }

    /**
     * @param id
     * @return List of Phones of the Person with requested id
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personPhones", produces = "application/json;charset=UTF-8")
    public List<Phone> personPhones(@RequestParam @NotNull Long id) {
        return phoneService.findPhonesByPersonId(id);
    }

    /**
     * @param id
     * @return List of Addresses of the person with requested id
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personAddresses", produces = "application/json;charset=UTF-8")
    public List<Address> personAddresses(@RequestParam @NotNull Long id) {
        return addressService.findAddressesByPersonId(id);
    }

    /**
     * @param id
     * @return List of Contacts of the Person with requested id
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personContacts", produces = "application/json;charset=UTF-8")
    public List<Contact> personContacts(@RequestParam @NotNull Long id) {
        return contactService.findContactsByPersonId(id);
    }

    /**
     * Update person info
     *
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/updatePersonInfo", produces = "application/json;charset=UTF-8")
    public StatusMessage updatePersonInfo(@ModelAttribute @Valid Person person, BindingResult result) throws ServletException {
        if (result.hasErrors()) {
            return INCORRECT_DATA;
        }
        if (!webUtil.getAuthAccount().getUsername().equals(person.getEmail())) {
            return AUTH_FAILED;
        }
        Person savedPerson = personService.findPersonByEmail(person.getEmail());
        savedPerson.setFirstName(person.getFirstName());
        savedPerson.setLastName(person.getLastName());
        savedPerson.setPatronymic(person.getPatronymic());
        savedPerson.setBirthday(person.getBirthday());
        savedPerson.setSex(person.getSex());
        savedPerson.setInfo(person.getInfo());
        try {
            personService.saveAndFlush(savedPerson);
            return SUCCESS;
        } catch (RuntimeException e) {
            log.debug(e.getMessage());
        }
        return SOMETHING_WRONG;
    }

    /**
     * Add or update phones for current person
     *
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/updatePersonPhones", produces = "application/json;charset=UTF-8")
    public StatusMessage updatePersonPhones(@RequestBody @NotNull ArrayList<PhoneDTO> phonesDTO) throws ServletException {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        for (PhoneDTO p : phonesDTO) {
            Set<ConstraintViolation<PhoneDTO>> constraintViolations = validator.validate(p);
            if (constraintViolations.size() > 0) {
                return INCORRECT_DATA;
            }
        }

        String email = phonesDTO.get(0).getPersonEmail(); // TODO: 17.10.2017 add email to json
        String validEmail = webUtil.getAuthAccount().getUsername();
        if (validEmail == null ||
                (email != null && !validEmail.equals(email))) {
            return AUTH_FAILED;
        }

        Person person = personService.findPersonByEmail(validEmail);
        if (person != null) {
            List<Phone> phones = phonesDTO.stream().map(PhoneDTO::getPhone).collect(Collectors.toList());
            try {
                phoneService.updatePhones(person, phones);
                return SUCCESS;
            } catch (RuntimeException e) {
                log.debug(e.getMessage());
            }
        }
        return SOMETHING_WRONG;
    }

    /**
     * Add or update contact for current person
     *
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/updatePersonContacts", produces = "application/json;charset=UTF-8")
    public StatusMessage updatePersonContacts(@RequestBody @NotNull @Valid ContactDTO dto, BindingResult result) throws ServletException {
        if (result.hasErrors()) {
            return INCORRECT_DATA;
        }
        if (!webUtil.getAuthAccount().getUsername().equals(dto.getPersonEmail())) {
            return AUTH_FAILED;
        }
        Person person = personService.findPersonByEmail(dto.getPersonEmail());
        if (person != null) {
            List<Contact> contacts = dto.getContacts();
            contacts.forEach(c -> c.setPerson(person));
            try {
                contactService.updateContacts(person, contacts);
                return SUCCESS;
            } catch (RuntimeException e) {
                log.debug(e.getMessage());
            }
        }
        return SOMETHING_WRONG;
    }

    /**
     * Add or update adress for current person
     *
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/updatePersonAddress", produces = "application/json;charset=UTF-8")
    public StatusMessage updatePersonAddress(@RequestHeader @NotNull String email,
                                             @RequestBody @NotNull @Valid AddressSimple address, // TODO: 23.09.2017 deal with Address.class to update description
                                             BindingResult result) throws ServletException {
        if (result.hasErrors()) {
            log.debug(String.valueOf(result));
            return INCORRECT_DATA;
        }
        if (!webUtil.getAuthAccount().getUsername().equals(email)) {
            return AUTH_FAILED;
        }
        Person person = personService.findPersonByEmail(email);
        if (person != null) {
            try {
                addressService.updateAddress(person, address);
                return SUCCESS;
            } catch (RuntimeException e) {
                log.debug(e.getMessage());
            }
        }
        return SOMETHING_WRONG;
    }

    /**
     * Delete address for current person
     *
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/deletePersonAddress", produces = "application/json;charset=UTF-8")
    public StatusMessage deletePersonAddress(@RequestHeader @NotNull String email,
                                             @RequestBody @NotNull AddressSimple address) throws ServletException {
        if (!webUtil.getAuthAccount().getUsername().equals(email)) {
            return AUTH_FAILED;
        }
        Person person = personService.findPersonByEmail(email);
        if (person != null && address.getId() != null) {
            try {
                addressService.deleteAddress(person, address);
                return SUCCESS;
            } catch (RuntimeException e) {
                log.debug(e.getMessage());
            }
        }
        return SOMETHING_WRONG;
    }

    /**
     * Generate string in XML format with data of the authorized Person
     *
     * @return String
     * @throws ServletException
     */
    @ResponseBody
    @GetMapping(value = "/public/api/exportPersonToXml", produces = "application/xml;charset=UTF-8")
    public String exportPersonToXML(HttpServletResponse response) throws ServletException {
        response.addHeader("Content-Disposition", "attachment; filename=person.xml");
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        try {
            return personService.jaxbParseToXml(person);
        } catch (ServiceException e) {
            throw new ServletException(e.getMessage(), e);
        }
    }

    /**
     * Import person data from xml
     *
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/importPersonFromXML", produces = "application/json;charset=UTF-8")
    protected StatusMessage importPersonFromXML(@RequestParam @NotNull MultipartFile file) throws ServletException {
        try {
            Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
            Person newPersonData = personService.jaxbParseFromXML(new String(file.getBytes()));
            personService.updatePerson(person, newPersonData);
            return SUCCESS;
        } catch (ServiceException | IOException e) {
            log.warn(e.getMessage());
        }
        return SOMETHING_WRONG;
    }

    /**
     * Add friend for authorized person
     */
    @ResponseBody
    @PostMapping(value = "/public/api/addFriend", produces = "application/json;charset=UTF-8")
    public void addFriend(@RequestParam("personId") @NotNull Long friendId) throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        personService.addFriend(person.getId(), friendId);
    }

    /**
     * Delete friend and friend requests between authorized person and other person
     */
    @Transactional
    @ResponseBody
    @PostMapping(value = "/public/api/deleteFriendsAndRequests", produces = "application/json;charset=UTF-8")
    public void deleteFriendsAndRequests(@RequestParam("friendId") @NotNull Long friendId) throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        personService.deleteFriends(person.getId(), friendId);
        personService.deleteFriendRequests(person.getId(), friendId);
    }

    /**
     * @return true if friend with id == personId already exists in person_friend table
     */
    @ResponseBody
    @PostMapping(value = "/public/api/checkFriend", produces = "application/json;charset=UTF-8")
    public boolean checkFriend(@RequestParam("personId") @NotNull Long friendId) throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        return person.getId().equals(friendId) ||
                personService.checkFriend(person.getId(), friendId);
    }

    /**
     * Add request for friendship
     */
    @ResponseBody
    @PostMapping(value = "/public/api/requestFriend", produces = "application/json;charset=UTF-8")
    public void friendRequest(@RequestParam("personId") @NotNull Long friendId) throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        if (person.getIncomeFriendRequest().stream().anyMatch(p -> p.getId().equals(friendId))) {
            personService.addFriend(person.getId(), friendId);
        } else {
            personService.addFriendRequest(person.getId(), friendId);
        }
    }

    /**
     * @return List of Friends (Persons) of the authorized Person
     */
    @ResponseBody
    @PostMapping(value = "/public/api/personFriends", produces = "application/json;charset=UTF-8")
    public List<Person> personFriends() throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        return person.getFriends();
    }

    /**
     * @return List of Persons who request for friendship with person with 'id'
     */
    @ResponseBody
    @PostMapping(value = "/public/api/incomeFriendRequest", produces = "application/json;charset=UTF-8")
    public List<Person> incomeFriendRequest() throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        return person.getIncomeFriendRequest();
    }

    /**
     * @return List of Persons who requested for friendship by person with 'id'
     */
    @ResponseBody
    @PostMapping(value = "/public/api/outcomeFriendRequest", produces = "application/json;charset=UTF-8")
    public List<Person> outcomeFriendRequest() throws ServletException {
        Person person = personService.findPersonByEmail(webUtil.getAuthAccount().getUsername());
        return person.getOutcomeFriendRequest();
    }
}
