package com.getjavajob.training.web1609.mamaeva.web.util;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;

@Slf4j
@Component
public class WebUtil {
    /**
     * @return authenticated Account
     * @throws ServletException
     */
    public Account getAuthAccount() throws ServletException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            String error = "auth == null";
            log.warn(error);
            throw new ServletException(error);
        }
        if (!(auth.getPrincipal() instanceof Account)) {
            String error = "auth.getPrincipal() not instance of Account.";
            log.warn(error);
            throw new ServletException(error);
        }
        return (Account) auth.getPrincipal();
    }
}
