package com.getjavajob.training.web1609.mamaeva.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EntityScan(basePackages = {"com.getjavajob.training.web1609.mamaeva.common"})
@EnableJpaRepositories(basePackages = {"com.getjavajob.training.web1609.mamaeva.dao"})
@EnableMongoRepositories(basePackages = {"com.getjavajob.training.web1609.mamaeva.dao"})
@ComponentScan(basePackages = {"com.getjavajob.training.web1609.mamaeva"})
@EnableWebMvc
@Slf4j
public class App extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(App.class);
    }
}
