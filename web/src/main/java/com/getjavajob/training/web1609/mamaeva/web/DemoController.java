package com.getjavajob.training.web1609.mamaeva.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class DemoController {

    @GetMapping("demo")
    public String helloAuth() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.debug(auth.getName() + " " + auth.getAuthorities() + " " + auth.isAuthenticated());
        return "demo/hello";
    }

    @GetMapping(value = {"demo/hello"})
    public String helloFreemarker(Model model) {
        model.addAttribute("integer_list", getIntegers());
        return "demo/hello";
    }

    @GetMapping(value = {"demo/jsp_hello"})
    public String helloJsp(Model model) {
        model.addAttribute("integer_list", getIntegers());
        return "jsp/demo/jsp_hello"; //jsp+jstl
    }

    @GetMapping(value = "demo/th_hello")
    public String helloThymeleaf(Model model) {
        model.addAttribute("integer_list", getIntegers());
        return "thymeleaf/demo/th_hello"; //Thymeleaf
    }

    private List<Integer> getIntegers() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10_000; i++) {
            list.add(i);
        }
        return list;
    }
}
