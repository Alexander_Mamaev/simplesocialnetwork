package com.getjavajob.training.web1609.mamaeva.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class ExpiredSessionFilter extends GenericFilterBean {

    /**
     * That filter verify if session is valid for ajax requests and set status code 500 for response if valid session not exists
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (req.getHeader("ajax") != null &&
                (req.getSession(false) == null || !req.isRequestedSessionIdValid())) {
            if (resp.getHeader("ajax_error") != null) {
                resp.setHeader("ajax_error", "SESSION_IS_EXPIRED");
            } else {
                resp.addHeader("ajax_error", "SESSION_IS_EXPIRED");
            }
            log.debug("EXPIRED_SESSION_FILTER --> req to " + req.getRequestURL());
            resp.setStatus(500);
            return;
        }
        filterChain.doFilter(request, response);
    }
}