package com.getjavajob.training.web1609.mamaeva.web.api;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.photos.Photo;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import com.getjavajob.training.web1609.mamaeva.service.PhotoService;
import com.getjavajob.training.web1609.mamaeva.service.ServiceException;
import com.getjavajob.training.web1609.mamaeva.web.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Controller
@MultipartConfig
public class AvatarApi {
    private final WebUtil webUtil;
    private final PersonService personService;
    private final PhotoService photoService;

    @Autowired
    public AvatarApi(WebUtil webUtil, PersonService personService, PhotoService photoService) {
        this.webUtil = webUtil;
        this.personService = personService;
        this.photoService = photoService;
    }

    /**
     * Save file (Photo) to the database
     *
     * @throws ServletException
     */
    @Transactional
    @RequestMapping(value = "/public/api/saveAvatar", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    protected void saveAvatar(@RequestParam @NotNull MultipartFile file) throws ServletException {
        Account account = webUtil.getAuthAccount();
        Photo photo;
        try {
            photo = photoService.savePhoto(account, file.getBytes());
        } catch (IOException e) {
            throw new ServletException(e.getMessage(), e);
        }
        Person person = personService.findPersonByEmail(account.getUsername());
        personService.setAvatar(person, photo);
    }

    /**
     * @return Photo or noname.png if avatar_id not found into the database
     * @throws ServletException
     */
    @ResponseBody
    @RequestMapping(value = "/public/api/getAvatar", method = RequestMethod.GET, produces = "image/png")
    public byte[] getAvatar(@RequestParam(required = false) @NotNull String avatar_id) throws ServletException {
        Photo photo = null;
        if (!StringUtils.isEmpty(avatar_id) && !avatar_id.equals("null")) {
            try {
                photo = photoService.findPhotoById(new ObjectId(avatar_id));
            } catch (ServiceException e) {
                throw new ServletException(e.getMessage(), e);
            }
        }
        return photo != null ? photo.getPhoto() : getNoNamePhoto();
    }

    /**
     * @return noname.png as byte[]
     * @throws ServletException
     */
    private byte[] getNoNamePhoto() throws ServletException {
        try (InputStream is = this.getClass().getClassLoader().getResourceAsStream("noName.png")) {
            return IOUtils.toByteArray(is);
        } catch (IOException e) {
            log.debug("can't load noName.png");
            throw new ServletException(e.getMessage(), e);
        }
    }
}
