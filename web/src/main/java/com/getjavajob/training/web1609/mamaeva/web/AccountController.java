package com.getjavajob.training.web1609.mamaeva.web;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.dto.AccountPersonDTO;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.service.AccountPersonService;
import com.getjavajob.training.web1609.mamaeva.service.AccountService;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class AccountController {
    private final AccountPersonService accountPersonService;
    private final PasswordEncoder passwordEncoder;
    private final AccountService accountService;
    private final PersonService personService;

    @Autowired
    public AccountController(AccountPersonService accountPersonService, PasswordEncoder passwordEncoder,
                             AccountService accountService, PersonService personService) {
        this.accountPersonService = accountPersonService;
        this.passwordEncoder = passwordEncoder;
        this.accountService = accountService;
        this.personService = personService;
    }

    /**
     * Allow bind iso-date format to LocalDate
     */
    @InitBinder
    public void customizeBinding(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, "birthday", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(LocalDate.parse(text, DateTimeFormatter.ISO_DATE));
            }
        });
    }

    @GetMapping(value = "/login")
    public String getLogin(@RequestParam(value = "error", required = false) String error,
                           @RequestParam(value = "logout", required = false) String logout,
                           Model model) {
        model.addAttribute("hasError", error != null);
        model.addAttribute("hasLogout", logout != null);
        return "jsp/login";
    }

    @GetMapping("/registerAccount")
    public String getRegister() {
        return "jsp/registerAccount";
    }

    /**
     * Save new Account and Person information from register page
     */
    @PostMapping("/registerAccount")
    protected String postRegister(@ModelAttribute @Valid AccountPersonDTO dto,
                                  BindingResult result,
                                  Model model) {
        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors().stream()
                    .map(e -> e.getField() + " : " + e.getDefaultMessage()).collect(Collectors.toList());
            model.addAttribute("fieldErrors", errors);
            model.addAttribute("dto", dto);
            log.debug("fieldErrors --> " + errors);
            return "jsp/registerAccount";
        }

        if (!dto.getPassword().equals(dto.getConfirmPassword())) {
            model.addAttribute("fieldErrors", "confirmPassword and Password don't match");
            model.addAttribute("dto", dto);
            return "jsp/registerAccount";
        }

        Account account = accountService.findAccountByUserName(dto.getEmail());
        Person person = personService.findPersonByEmail(dto.getEmail());
        if (account != null || person != null) {
            model.addAttribute("accountExists", true);
            return "jsp/registerAccount";
        }

        dto.setPassword(passwordEncoder.encode(dto.getPassword()));
        account = accountPersonService.registerAccount(dto);

        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(account, account.getPassword(), account.getAuthorities()));
        return "redirect:/public/main";
    }

    @GetMapping("/public/editAccount")
    public String editAccount() throws ServletException {
        return "jsp/templates/editAccount";
    }
}
