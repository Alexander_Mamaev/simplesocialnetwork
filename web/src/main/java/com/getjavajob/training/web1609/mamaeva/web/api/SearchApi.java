package com.getjavajob.training.web1609.mamaeva.web.api;

import com.getjavajob.training.web1609.mamaeva.common.group.Group;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.service.GroupService;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class SearchApi {
    private final PersonService personService;
    private final GroupService groupService;

    @Autowired
    public SearchApi(PersonService personService, GroupService groupService) {
        this.personService = personService;
        this.groupService = groupService;
    }

    @ResponseBody
    @PostMapping(value = "/public/api/searchBar", produces = "application/json;charset=UTF-8")
    public ResponseEntity<List<EntityWrapper>> searchBar(@RequestParam @NotNull String searchQuery,
                                                         @RequestParam @NotNull Integer limit) throws ServletException {
        List<Person> persons = personService.searchBar(searchQuery, limit);
        List<Group> groups = groupService.searchBar(searchQuery, limit);
        List<EntityWrapper> wrappers = new ArrayList<>();
        for (Person p : persons) {
            wrappers.add(new EntityWrapper(p.getId(), p.getFirstName() + " " + p.getLastName(), "person"));
        }
        for (Group g : groups) {
            wrappers.add(new EntityWrapper(g.getId(), g.getName(), "group"));
        }
        return new ResponseEntity<>(wrappers, HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping(value = "/public/api/searchPersonsWithQuery", produces = "application/json;charset=UTF-8")
    public List<Person> searchPersons(@RequestParam @NotNull String searchQuery,
                                      @RequestParam @NotNull Integer page,
                                      @RequestParam @NotNull Integer size) {
        return personService.findWithQueryMatch(searchQuery, searchQuery, new PageRequest(page, size));
    }

    @ResponseBody
    @PostMapping(value = "/public/api/countPersonByQuery", produces = "application/json;charset=UTF-8")
    public Integer countPersonByQuery(@RequestParam @NotNull String searchQuery) {
        return personService.countPersonWithQueryMatch(searchQuery, searchQuery);
    }

    private class EntityWrapper {
        private long id;
        private String name;
        private String type;

        public EntityWrapper(long id, String name, String type) {
            this.id = id;
            this.name = name;
            this.type = type;
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }
    }
}

