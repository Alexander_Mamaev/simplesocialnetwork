package com.getjavajob.training.web1609.mamaeva.web.api;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.message.ChatMessage;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.dao.MessageDao;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class ChatApi {
    private final MessageDao messageDao;
    private final PersonService personService;

    @Autowired
    public ChatApi(MessageDao messageDao, PersonService personService) {
        this.messageDao = messageDao;
        this.personService = personService;
    }

    @MessageMapping({"/send_message"})
    @SendTo("/broker/messaging")
    public Map<String, String> messaging(@NotNull Map<String, String> message, @NotNull Authentication auth) {
        Account account = (Account) auth.getPrincipal();
        ChatMessage chatMessage = ChatMessage
                .builder()
                .account(account)
                .message(message.get("message"))
                .time(LocalDateTime.now().toInstant(ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).toEpochMilli())
                .build();
        messageDao.save(chatMessage);
        return chatMessageDto(chatMessage);
    }

    @ResponseBody
    @PostMapping(value = "/public/api/commonChat/lastRecords", produces = "application/json")
    public List<Map<String, String>> lastMessageRecords(@RequestParam @NotNull Integer page, @RequestParam @NotNull Integer size) {
        List<ChatMessage> list = messageDao.findTopByOrderByTimeDesc(new PageRequest(page, size));
        return list.stream().map(this::chatMessageDto).collect(Collectors.toCollection(LinkedList::new));
    }

    private Map<String, String> chatMessageDto(ChatMessage chatMessage) {
        Person person = personService.findPersonByEmail(chatMessage.getAccount().getUsername()); // TODO: 28.08.2017 if person deleted? check null or Optional
        Map<String, String> map = new HashMap<>();
        map.put("name", person.getFirstName() + " " + person.getLastName());
        map.put("time", String.valueOf(chatMessage.getTime()));
        map.put("message", chatMessage.getMessage());
        return map;
    }
}