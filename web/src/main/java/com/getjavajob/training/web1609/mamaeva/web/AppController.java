package com.getjavajob.training.web1609.mamaeva.web;

import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import com.getjavajob.training.web1609.mamaeva.web.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.ServletException;

@Controller
@Slf4j
public class AppController {
    private final WebUtil webUtil;
    private final PersonService personService;

    @Autowired
    public AppController(WebUtil webUtil, PersonService personService) {
        this.webUtil = webUtil;
        this.personService = personService;
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/public/main";
    }

    @GetMapping("/public/main")
    public String personalPage(Model model) throws ServletException {
        String userName = webUtil.getAuthAccount().getUsername();
        Person person = personService.findPersonByEmail(userName);
        model.addAttribute("personId", person.getId());
        model.addAttribute("userName", userName);
        log.debug("Account successfully logged in: " + userName);
        return "jsp/public/main";
    }

    @GetMapping("/public/homeContent")
    public String homeContent() {
        return "jsp/templates/homeContent";
    }

    @GetMapping("/public/friendsContent")
    public String homeFriendsContent() {
        return "jsp/templates/friendsContent";
    }

    @GetMapping("/public/searchContent")
    public String searchContent() {
        return "jsp/templates/searchContent";
    }

    @GetMapping("/public/avatar")
    public String getAvatar() {
        return "jsp/public/submitAvatar";
    }

    @GetMapping("/public/templates/common_chat")
    public String commonChat() {
        return "jsp/templates/common_chat";
    }
}

