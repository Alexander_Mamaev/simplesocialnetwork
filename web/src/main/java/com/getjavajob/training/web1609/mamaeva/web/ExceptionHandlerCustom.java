package com.getjavajob.training.web1609.mamaeva.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerCustom {

    /**
     * Create view of error message
     */
    @ExceptionHandler(Exception.class)
    public String error(Model model, HttpServletRequest request, HttpServletResponse response, Exception e) {
        response.setStatus(500);
        String errorMessage = buildErrorMessage(request, response, e).toString();
        model.addAttribute("errorMessage", errorMessage);
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        log.warn(String.valueOf(errors));
        return "jsp/errors/error";
    }

    /**
     * Create view of error message if no handler found
     */
    @ExceptionHandler(NoHandlerFoundException.class)
    public String handleError(Model model, HttpServletRequest request, HttpServletResponse response, Exception e) {
        String errorMessage = buildErrorMessage(request, response, e).toString();
        log.warn(errorMessage);
        model.addAttribute("errorMessage", errorMessage);
        return "jsp/errors/NoHandlerFoundException";
    }

    private StringBuilder buildErrorMessage(HttpServletRequest request, HttpServletResponse response, Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return new StringBuilder()
                .append("No handler found exception").append("</br>")
                .append("Request url: ").append(request.getRequestURL()).append("</br>")
                .append("Response status: ").append(response.getStatus()).append("</br>")
                .append("Error: ").append(e);
    }
}
