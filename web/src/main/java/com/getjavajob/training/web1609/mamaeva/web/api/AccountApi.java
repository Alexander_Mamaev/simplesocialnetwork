package com.getjavajob.training.web1609.mamaeva.web.api;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.message.StatusMessage;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.service.AccountPersonService;
import com.getjavajob.training.web1609.mamaeva.service.AccountService;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import com.getjavajob.training.web1609.mamaeva.web.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.validation.constraints.NotNull;

import static com.getjavajob.training.web1609.mamaeva.common.message.StatusMessage.*;

@Slf4j
@Controller
public class AccountApi {
    private final WebUtil webUtil;
    private final PasswordEncoder passwordEncoder;
    private final AccountService accountService;
    private final PersonService personService;
    private final AccountPersonService accountPersonService;

    @Autowired
    public AccountApi(WebUtil webUtil, PasswordEncoder passwordEncoder, AccountService accountService, PersonService personService, AccountPersonService accountPersonService) {
        this.webUtil = webUtil;
        this.passwordEncoder = passwordEncoder;
        this.accountService = accountService;
        this.personService = personService;
        this.accountPersonService = accountPersonService;
    }

    /**
     * Check if authentication still exists
     *
     * @return authenticated user name
     * @throws ServletException
     */
    @ResponseBody
    @GetMapping("/public/api/checkAuth")
    public String checkAuthExists() throws ServletException {
        return webUtil.getAuthAccount().getUsername();
    }

    /**
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/updateAccountEmail", produces = "application/json")
    public StatusMessage updateAccountEmail(@RequestParam @NotNull String email,
                                            @RequestParam @NotNull String password) throws ServletException {
        Account account = webUtil.getAuthAccount();
        Person person = personService.findPersonByEmail(account.getUsername());
        log.debug(account.getUsername() + " --> " + email);
        if (!email.equals(account.getUsername())) {
            if (!passwordEncoder.matches(password, account.getPassword())) {
                return WRONG_PASSWORD;
            }
            if (accountService.findAccountByUserName(email) != null) {
                return EMAIL_EXISTS;
            }
            if (person != null) {
                try {
                    accountPersonService.updateAccountEmail(email, account, person);
                } catch (RuntimeException e) {
                    log.debug(e.getMessage());
                    return ERROR;
                }
                return SUCCESS;
            } else {
                throw new ServletException("person with old email not find in BD");
            }
        }
        return NOT_REQUIRED;
    }

    /**
     * @return StatusMessage with response status code and message
     */
    @ResponseBody
    @PostMapping(value = "/public/api/updateAccountPassword", produces = "application/json")
    public StatusMessage updateAccountPassword(@RequestParam @NotNull String oldPassword,
                                     @RequestParam @NotNull String newPassword,
                                     @RequestParam @NotNull String confirmPassword) throws ServletException {
        Account account = webUtil.getAuthAccount();
        if (!newPassword.equals(confirmPassword)) {
            return NOT_MATCHED;
        }
        if (!passwordEncoder.matches(oldPassword, account.getPassword())) {
            return WRONG_PASSWORD;
        }
        account.setPassword(passwordEncoder.encode(newPassword));
        accountService.save(account);
        return SUCCESS;
    }
}
