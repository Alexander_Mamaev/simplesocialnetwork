<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="templates/meta.jsp"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3" style="margin-top:45px">
            <h2 class="text-info" style="text-align:center">Welcome</h2>
            <div id="login-panel" class="panel panel-default">
                <div class="panel-body">
                    <c:if test="${hasLogout}">
                        <div class="alert alert-info" role="alert">You've been logged out successfully.</div>
                    </c:if>
                    <c:if test="${hasError}">
                        <div class="alert alert-danger" role="alert">Invalid Username or Password!</div>
                    </c:if>

                    <form method="post" action="${pageContext.request.contextPath}/login" data-toggle="validator">
                        <div class="form-group has-feedback">
                            <label class="text-info" for="username">Username</label>
                            <input type="email" class="form-control" id="username" placeholder="Username"
                                   name="username"
                                   pattern="^(?:[a-zA-Z0-9_'^&/+-])+(?:\.(?:[a-zA-Z0-9_'^&/+-])+)*@(?:(?:\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\.){3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\]?)|(?:[a-zA-Z0-9-]+\.)+(?:[a-zA-Z]){2,}\.?)$"
                                   required>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="text-info" for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password"
                                   name="password"
                                   data-minlength="8"
                                   required>
                            <span class="glyphicon form-control-feedback"></span>
                        </div>
                        <div style="text-align:center">
                            <div class="checkbox">
                                <label><input type="checkbox" name="remember-me" id="remember_me">
                                    <span class="text-info">remember me</span></label>
                            </div>

                            <input name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
                            <button class="btn btn-default btn-lg">
                                <span class="text-info">Log in</span>
                            </button>

                            <div style="margin-top:20px">
                                <a href="registerAccount" class="text-info"><b><strong>Register</strong></b></a>&emsp;
                                <a href="#" class="text-info" data-toggle="popover" data-placement="right"
                                   data-content="ой, зря вы так!">Forgot password?</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>var contextPath = "${pageContext.request.contextPath}"</script>
</body>
</html>
