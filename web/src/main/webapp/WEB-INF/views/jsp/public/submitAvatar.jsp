<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body>
<form id="submitAvatarForm" action="/public/api/saveAvatar" method="post"
      enctype="multipart/form-data">
    <input id="avatar" type="file" name="file">
    <input name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
    <input id="submitAvatar" type="submit" value="Submit">
</form>
</body>
</html>
