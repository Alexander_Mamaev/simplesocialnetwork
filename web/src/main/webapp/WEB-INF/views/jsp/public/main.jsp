<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <jsp:include page="../templates/meta.jsp"/>
</head>
<body>
<jsp:include page="../templates/navBar.jsp"/>

<div id="main-content" class="container">
    <div class="col-sm-2">
        <div class="whitesmoke common-font">
            <button type="button" class="btn btn-primary btn-block" id="homeButton" onclick="homeContent()">Home
            </button>
            <button type="button" class="btn btn-primary btn-block" id="friendsButton" onclick="friendsContent()">
                Friends
            </button>
            <button type="button" class="btn btn-primary btn-block" id="groupsButton" disabled>Groups</button>
            <button type="button" class="btn btn-primary btn-block" id="photosButton" disabled>Photos</button>
            <button type="button" class="btn btn-primary btn-block" id="messagesButton" disabled>Messages</button>
            <button type="button" class="btn btn-primary btn-block" id="chatButton" onclick="chatContent()">Chat
            </button>
            <button type="button" class="btn btn-primary btn-block" id="newsButton" onclick="newsContent()" disabled>
                News
            </button>
        </div>
    </div>

    <div class="col-sm-8">
        <div id="dynamic-content"></div>
    </div>

    <div class="col-sm-2">
        <div id="right-content">
        </div>
    </div>

</div>
<script>var contextPath = "${pageContext.request.contextPath}"</script>
<script>var userName = "${userName}"</script>
<script>var personId = "${personId}"</script>
<script>var csrf_token = "${_csrf.token}"</script>
<script src="${pageContext.request.contextPath}/resources/js/friendList.js"></script>
<script src="${pageContext.request.contextPath}/webjars/sockjs-client/sockjs.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/stomp-websocket/stomp.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/chat.js"></script>
</body>
</html>
