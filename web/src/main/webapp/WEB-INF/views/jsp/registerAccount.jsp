<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="templates/meta.jsp"/>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="margin-top:45px">
            <h2 class="text-info" style="text-align:center">Please register</h2>
            <div id="login-panel" class="panel panel-default">
                <div class="panel-body">

                    <c:if test="${accountExists}">
                        <div class="alert alert-danger" role="alert">Account already exists.</div>
                    </c:if>

                    <c:if test="${not empty fieldErrors}">
                        <div class="alert alert-danger" role="alert"> Please fill the fields correctly!</div>
                        <div class="alert alert-danger" role="alert" style="margin-top:20px">
                            <c:forEach var="error" items="${fieldErrors}">
                                <p>${error}</p>
                            </c:forEach>
                        </div>

                    </c:if>

                    <form class="form-horizontal" action="registerAccount" method="post" data-toggle="validator">
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="firstName">First name:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="firstName" name="firstName" value="${dto.firstName}"
                                       pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="lastName">Last name:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="lastName" name="lastName" value="${dto.lastName}"
                                       pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="patronymic">Patronymic:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="patronymic" name="patronymic" value="${dto.patronymic}"
                                       pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>

                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="email">Email:</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="email" name="email" value="${dto.email}"
                                       pattern="^(?:[a-zA-Z0-9_'^&/+-])+(?:\.(?:[a-zA-Z0-9_'^&/+-])+)*@(?:(?:\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\.){3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\]?)|(?:[a-zA-Z0-9-]+\.)+(?:[a-zA-Z]){2,}\.?)$"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="sex">Sex:</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="sex" name="sex" required>
                                    <c:choose>
                                        <c:when test="${dto.sex == 'F'}">
                                            <option value="M">Male</option>
                                            <option value="F" selected>Female</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="M" selected>Male</option>
                                            <option value="F">Female</option>
                                        </c:otherwise>
                                    </c:choose>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="datepicker">Birthday:</label>
                            <div class="col-sm-8">
                                <input class="form-control date-picker" id="datepicker" name="birthday"
                                       placeholder="YYYY-MM-DD" value="${dto.birthday}"
                                       pattern="^\d{4}-\d{2}-\d{2}$"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="password">Password:</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="password" name="password"
                                       data-minlength="8"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label class="control-label col-sm-4 text-info" for="confirmPassword">Confirm
                                password:</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="confirmPassword" name="confirmPassword"
                                       data-minlength="8"
                                       required>
                                <span class="glyphicon form-control-feedback"></span>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <input name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
                        <div style="text-align:center; margin-top:45px">
                            <button class="btn btn-default btn-lg">
                                <span class="text-info">Register</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>var contextPath = "${pageContext.request.contextPath}"</script>
</body>
</html>