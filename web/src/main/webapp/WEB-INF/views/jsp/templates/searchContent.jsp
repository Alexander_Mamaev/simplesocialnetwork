<div>
    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a class="check" data-toggle="tab" href="#menuPersons" onclick="loadFriendList()">Persons</a>
        </li>
        <li><a class="check" data-toggle="tab" href="#menuGroups">Groups</a></li>
    </ul>
    <div class="tab-content">
        <%--Persons--%>
        <div id="menuPersons" class="tab-pane fade in active">
            <div class="col-sm-10 col-sm-offset-1" style="margin-top:25px">
                <div class="list-group" id="personsList">
                </div>
                <div class="list-group" id="">
                    <button type="button" class="btn btn-link center-block" id="morePersons">more</button>
                </div>
            </div>

        </div>
        <%--Groups--%>
        <div id="menuGroups" class="tab-pane fade">
            <div class="col-sm-10 col-md-offset-1" style="margin-top:25px">
                <div class="list-group" id="groupsList">
                    <div class="text-center"><p>Not supported yet...</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
