<title>mamaevas</title>
<script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/jquery-ui/jquery-ui.css">

<link href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.min.js"></script>
<link href="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"
      rel="stylesheet">
<script src="${pageContext.request.contextPath}/webjars/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/webjars/bootstrap-validator/js/validator.js"></script>

<link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>

<link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/icons/mas.ico"/>

<meta name="viewport" content="width=device-width, initial-scale=1">
