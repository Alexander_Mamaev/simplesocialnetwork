<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websocket relies on Javascript being
    enabled. Please enable Javascript and reload this page!</h2></noscript>

<div class="panel panel-default">
    <div class="panel-body chat-panel" id="chat_panel">
    </div>
    <div class="panel-footer">
        <div class="form-group">
                    <textarea path="info" class="form-control" rows="3" id="messageTextArea"
                              name="messageTextArea"></textarea>
            <p>Enter - send message. Ctrl+Enter - new line</p>
        </div>
    </div>
</div>