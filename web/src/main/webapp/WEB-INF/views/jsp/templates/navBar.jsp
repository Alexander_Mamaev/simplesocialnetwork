<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<head>
    <link href="${pageContext.request.contextPath}/resources/css/navBar.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Название компании и кнопка, которая отображается для мобильных устройств группируются для лучшего отображения при свертывании -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}/public/main">MAMAEVAS</a>
        </div>


        <!-- Группируем ссылки, формы, выпадающее меню и прочие элементы -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">


            </ul>


            <ul class="nav navbar-nav navbar-center">
                <form class="navbar-form navbar-left" role="search" method="post">
                    <div class="form-group">
                        <input type="text" id="searchQuery" name="searchQuery" class="form-control"
                               placeholder="Search">
                    </div>
                    <button type="submit" id="searchButton" class="btn btn-default">
                        <i class="glyphicon glyphicon-search" style="font-size: 19px; color: #555555"></i>
                    </button>
                </form>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><strong>menu</strong><b
                            class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" id="editAccountLink" onclick="loadEditAccountLinkContent()">Edit account</a>
                        </li>
                        <%--<li><a href="#">Create group</a></li>--%>
                        <%--<li><a href="#">Something else here</a></li>--%>
                        <li class="divider"></li>
                        <%--<li><a href="#">Separated link</a></li>--%>
                        <li class="divider"></li>
                        <li><a href="${pageContext.request.contextPath}/logout">Logout</a></li>
                    </ul>
                </li>
                <p class="navbar-text">${userName}</p>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<script src="${pageContext.request.contextPath}/resources/js/forSearchBar.js"></script>
</body>