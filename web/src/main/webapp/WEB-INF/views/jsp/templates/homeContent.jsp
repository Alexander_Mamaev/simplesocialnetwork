<div class="col-sm-4 whitesmoke common-font">
    <div class="panel panel-default noborder-single" id="avatar">
        <%--Avatar Here--%>
    </div>

    <button type="button" class="btn btn-primary center-block" id="becomeFriendsButton" style="display: none;">Make
        friends
    </button>

</div>
<div class="col-sm-8">
    <div class="panel-group common-font" id="accordion">
        <div class="panel panel-default whitesmoke">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse1"><strong class="primary-color">Personal
                        information</strong></a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <table class="table-condensed common-font noborder">
                    <tbody id="personalInfo"></tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default whitesmoke">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse2"><strong class="primary-color">Phones</strong></a>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <table class="table-condensed common-font noborder">
                    <tbody id="phones"></tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default whitesmoke">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse3"><strong class="primary-color">Addresses</strong></a>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <table class="table-condensed common-font noborder">
                    <tbody id="addresses"></tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default whitesmoke">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapse4"><strong class="primary-color">Contacts</strong></a>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <table class="table-condensed common-font noborder">
                    <tbody id="contacts"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
