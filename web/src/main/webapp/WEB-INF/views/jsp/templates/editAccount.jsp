<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div id="editAccountDiv">
    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a class="check" data-toggle="tab" href="#menuAccount">Account</a></li>
        <li><a class="check" data-toggle="tab" href="#menuPassword">Password</a></li>
        <li><a class="check" data-toggle="tab" href="#menuPerson" onclick="loadPersonEditInfo()">Person</a></li>
        <li><a class="check" data-toggle="tab" href="#menuAvatar" onclick="loadPersonEditAvatar()">Avatar</a></li>
        <li><a class="check" data-toggle="tab" href="#menuXml">XML</a></li>
    </ul>

    <div class="tab-content">
        <%--Account--%>
        <div id="menuAccount" class="tab-pane fade in active">
            <div class="col-sm-10 col-sm-offset-1" style="margin-top:25px">
                <form class="form-horizontal" data-toggle="validator">
                    <div class="form-group has-feedback">
                        <label class="text-info" for="accountUsername">Email</label>
                        <input class="form-control" id="accountUsername" name="accountUsername" value=""
                               pattern="^(?:[a-zA-Z0-9_'^&/+-])+(?:\.(?:[a-zA-Z0-9_'^&/+-])+)*@(?:(?:\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\.){3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\]?)|(?:[a-zA-Z0-9-]+\.)+(?:[a-zA-Z]){2,}\.?)$"
                               data-error="Bruh, that email address is invalid"
                               required>
                        <span class="glyphicon form-control-feedback"></span>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group has-feedback">
                        <label class="text-info" for="accountPassword">Password</label>
                        <input type="password" class="form-control" id="accountPassword" name="accountPassword"
                               data-error="Minimum of 8 characters"
                               data-minlength="8"
                               required>
                        <span class="glyphicon form-control-feedback"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div style="text-align: center">
                        <button class="btn btn-default" id="updateAccountSubmitButton" style="margin-top:25px">
                            <span class="text-info">Save</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <%--Password--%>
        <div id="menuPassword" class="tab-pane fade">
            <div class="col-sm-10 col-md-offset-1" style="margin-top:25px">
                <form class="form-horizontal" data-toggle="validator">
                    <div class="form-group has-feedback">
                        <label class="text-info" for="oldPassword">Old password</label>
                        <input class="form-control" id="oldPassword" name="oldPassword" value=""
                               data-error="Minimum of 8 characters"
                               data-minlength="8"
                               required>
                        <span class="glyphicon form-control-feedback"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="text-info" for="newPassword">New password</label>
                        <input type="password" class="form-control" id="newPassword" name="newPassword"
                               data-error="Minimum of 8 characters"
                               data-minlength="8"
                               required>
                        <span class="glyphicon form-control-feedback"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="text-info" for="confirmPassword">Confirm password</label>
                        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword"
                               data-error="Minimum of 8 characters"
                               data-minlength="8"
                               required>
                        <span class="glyphicon form-control-feedback"></span>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div style="text-align: center">
                        <button class="btn btn-default" id="updatePasswordSubmitButton" style="margin-top:25px">
                            <span class="text-info">Save</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <%--Person--%>
        <div id="menuPerson" class="tab-pane fade">
            <div class="col-sm-12">
                <ul class="nav nav-pills nav-justified">
                    <li class="active"><a data-toggle="pill" href="#menuInformation" onclick="loadPersonEditInfo()">Information</a>
                    </li>
                    <li><a data-toggle="pill" href="#menuPhones" onclick="loadPersonEditPhones()">Phones</a></li>
                    <li><a data-toggle="pill" href="#menuAddresses" onclick="loadPersonEditAddresses()">Addresses</a>
                    </li>
                    <li><a data-toggle="pill" href="#menuContacts" onclick="loadPersonEditContacts()">Contacts</a></li>
                </ul>

                <div class="tab-content" style="margin-top:20px">
                    <div id="menuInformation" class="tab-pane fade in active">
                        <%--Personal info--%>
                    </div>
                    <div id="menuPhones" class="tab-pane fade text-center">
                        <form id="personPhonesForm" name="personPhonesForm" data-toggle="validator">
                            <div id="personPhonesFormContent">
                                <%--Phone content--%>
                            </div>
                            <div id="addPhoneButton" class="form-group custom-buttons-1 addPhoneButton">Add new phone
                            </div>
                            <button class="btn btn-default" id="updatePhonesSubmitButton" style="margin-top:10px">
                                <span class="text-info">Save</span>
                            </button>
                        </form>
                    </div>
                    <div id="menuAddresses" class="tab-pane fade text-center">
                        <div id="personAddressFormContent">
                            <%--addresses content--%>
                        </div>
                        <div id="addAddressButton" class="form-group custom-buttons-1" style="margin-top:10px">
                            Add new address
                        </div>
                    </div>
                    <div id="menuContacts" class="tab-pane fade text-center">
                        <div id="personContactsFormContent">
                            <%--contacts content--%>
                        </div>
                        <div id="addContactButton" class="form-group custom-buttons-1 addContactButton">Add new
                            contact
                        </div>
                        <button class="btn btn-default" id="updateContactSubmitButton" style="margin-top:10px">
                            <span class="text-info">Save</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <%--Avatar--%>
        <div id="menuAvatar" class="tab-pane fade">
            <div class="col-sm-10 col-sm-offset-1" style="margin-top:25px">
                <form id="avatarForm">
                    <img id="avatarImg" class="img-responsive img-rounded center-block"
                         style="max-height: 200px; max-width: 200px">
                    <input id="avatarFile" type="file" name="file" style="display:none">
                    <div id="selectAvatar" class="form-group custom-buttons-1 addPhoneButton center-block"
                         style="width: 90px; margin-top:30px">Select avatar
                    </div>
                    <button id="changeAvatarButton" class="btn btn-default center-block" style="margin-top:20px">
                        <span class="text-info">Save</span></button>
                </form>
            </div>
        </div>
        <%--XML--%>
        <div id="menuXml" class="tab-pane fade">
            <div class="col-sm-10 col-sm-offset-1" style="margin-top:25px">
                <a href="${pageContext.request.contextPath}/public/api/exportPersonToXml"
                   id="linkExportToXML" style="text-align:center;display:block;">Export to XML file</a>
                <form id="importXMLForm">
                    <input id="xmlFile" type="file" name="file" style="display:none">
                    <a href="#"
                       id="importXmlFileLink" style="text-align:center;display:block;margin-top:25px">Import from XML file</a>
                </form>

            </div>
        </div>

    </div>

</div>

<!-- Modal -->
<div class=" modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p style="text-align: center" id="modalMessage"></p>
            </div>
            <div class="modal-footer">
                <button id="myModalButton" type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- Address Modal -->
<div class="modal fade" id="addressModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" id="#addressModalBody">

            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/resources/js/editAccount.js"></script>
</body>