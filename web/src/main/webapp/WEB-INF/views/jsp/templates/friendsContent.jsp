<div>
    <ul class="nav nav-tabs nav-justified">
        <li class="active"><a class="check" data-toggle="tab" href="#menuFriends" onclick="loadFriendList()">Friends</a>
        </li>
        <li><a class="check" data-toggle="tab" href="#menuIncome" onclick="incomeContent()">Income</a></li>
        <li><a class="check" data-toggle="tab" href="#menuOutcome" onclick="outcomeContent()">Outcome</a></li>
    </ul>
    <div class="tab-content">
        <%--Friends--%>
        <div id="menuFriends" class="tab-pane fade in active">
            <div class="col-sm-10 col-sm-offset-1" style="margin-top:25px">
                <div class="list-group" id="friendsList">
                </div>
            </div>
        </div>
        <%--Income--%>
        <div id="menuIncome" class="tab-pane fade">
            <div class="col-sm-10 col-md-offset-1" style="margin-top:25px">
                <div class="list-group" id="incomeList">
                </div>
            </div>
        </div>
        <%--Outcome--%>
        <div id="menuOutcome" class="tab-pane fade">
            <div class="col-sm-10 col-md-offset-1" style="margin-top:25px">
                <div class="list-group" id="outcomeList">
                </div>
            </div>
        </div>
    </div>
</div>