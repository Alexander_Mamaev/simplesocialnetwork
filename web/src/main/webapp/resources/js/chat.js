var stompClient = null;
var socket = null;
var chatPage = 0;
var messagesForOnePage = 20;


function chatConnect() {
    socket = new SockJS(contextPath + '/common_chat_ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/broker/messaging', function (messaging) {
            appendMessage(JSON.parse(messaging.body));
            chatAutoScrollMessageDown();
        });
    }, function () {
        window.location = contextPath + "/";
    });

    $("#chat_panel").on("remove", function () {
        stompClient.disconnect();
    });
}

function sendMessage() {
    var tmp = $("#messageTextArea").val().replace(/\r\n|\r|\n/g, "");
    if (!tmp.trim()) {
        return false;
    }
    var message = $("#messageTextArea").val().replace(/\r\n|\r|\n/g, "<br/>");
    $.get(contextPath + '/public/api/checkAuth')
        .done(function (data) {
            stompClient.send("/wsApp/send_message", {},
                JSON.stringify({
                    'message': message
                }));
        })
}

function enterKeyUpdater(e) {
    // Ctrl-Enter pressed
    if (e.ctrlKey && e.keyCode === 13) {
        var textArea = $("#messageTextArea");
        textArea.val(textArea.val() + "\n");
        return false;
    }
    // Enter pressed
    if (e.keyCode === 13) {
        sendMessage();
        $("#messageTextArea").val('');
        return false;
    }
}

function pagingMessages(page, size) {
    $.ajax({
        url: contextPath + '/public/api/commonChat/lastRecords',
        data: {
            page: page,
            size: size
        },
        method: "POST",
        success:
            function (data) {
                var $current_top_element = $('#chat_panel').children().first();
                var previous_height = 0;

                $.each(data, function (index, message) {
                    prependMessage(message);
                });

                $current_top_element.prevAll().each(function () {
                    previous_height += $(this).outerHeight();
                });

                $('#chat_panel').scrollTop(previous_height);

                if (page === 0) {
                    chatAutoScrollMessageDown();
                }
            }
    });
}

function chatAutoScrollMessageDown() {
    $('#chat_panel').animate({scrollTop: $('#chat_panel').prop("scrollHeight")}, 'slow');
}

function prependMessage(message) {
    var date = new Date(parseInt(message["time"]));
    var m = single_message(date, message);
    $('#chat_panel').prepend(m);
}

function appendMessage(message) {
    var date = new Date(parseInt(message["time"]));
    var m = single_message(date, message);
    $('#chat_panel').append(m);
}

function single_message(date, message) {
    return '<div class="single_message">' +
        '<div>' +
        '<span style="font-size:12px; font-weight:bold">' + message["name"] + '</span>' +
        '&nbsp' +
        '<span style="color:#5e5e5e; font-size:10px; font-weight: bold">' +
        date.getDate() + '.' +
        (date.getMonth() + 1) + '.' +
        date.getFullYear() + '&nbsp' +
        date.getHours() + ":" +
        date.getMinutes() + ":" +
        date.getSeconds() +
        '</span>' +
        '</div>' +
        '<span>' + message["message"] + '</span>' +
        '</div>' +
        '</br>';
}

function scrollUpMessageListener() {
    $('#chat_panel').scroll(function () {
        if ($(this).scrollTop() === 0) {
            pagingMessages(chatPage++, messagesForOnePage);
        }
    });
}

