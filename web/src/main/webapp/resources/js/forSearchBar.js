$(document).ready(function () {
    $("#searchQuery").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: contextPath + '/public/api/searchBar',
                data: {
                    searchQuery: request.term,
                    limit: 5 //limit top 5 founded elements
                },
                method: "POST",
                success: function (data) {
                    response($.map(data, function (entity, i) {
                        if (i > 9) return;
                        return {
                            value: request.term,
                            label: i + 1 + '. ' + entity.name,
                            id: entity.id,
                            type: entity.type
                        };
                    }));
                }
            });
        },
        minLength: 1,
        select: function (event, ui) {
            if (ui.item.type === "person") {
                loadHomeContent(ui.item.id);
                $(this).val("");
                return false;
            } else if (ui.item.type === "group") {
                alert("not supported yet");
            } else {
                alert("something wrong in response parameters type");
            }
        }
    });

    $("#avatarNavBar").on('click', function () {
        window.location.href = "personalPageController";
    });

    $('#searchButton').on('click', function (e) {
        e.preventDefault();
        loadSearchContent($('#searchQuery').val(), 0, sizeOfSearch);
        $('#searchQuery').val("");
    })
});

function postToUrl(path, params, method) {
    method = method || "post"; // Устанавливаем метод отправки.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    for (var key in params) {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);
        form.appendChild(hiddenField);
    }
    document.body.appendChild(form);
    form.submit();
}