var currentPerson;
var addressSimple;
var $modal = $('#addressModal');

$(document).ready(function () {
    $('#editAccountDiv').validator('update')
});


// ------------ Update Account Email ------------
$(document).ready(function () {
    $('#updateAccountSubmitButton').on('click', function (e) {
        e.preventDefault();
        if ($('#menuAccount').validator('validate').has('.has-error').length === 0) {
            updateAccountEmail(
                $('#accountUsername').val(),
                $('#accountPassword').val())
        }
    });
});

function updateAccountEmail(email, password) {
    var d = $.Deferred();
    var post = $.ajax({
        url: contextPath + '/public/api/updateAccountEmail',
        data: {
            email: email,
            password: password
        },
        method: "POST"
    });

    post.done(function (responseStatus) {
        var message;
        switch (responseStatus.status) {
            case 904:
                message = "error occurred";
                break;
            case 901:
                message = "Successfully updated";
                userName = email;
                break;
            case 905:
                message = "Email already exists";
                break;
            case 906:
                message = "Wrong password";
                break;
            case 907:
                message = "No update required";
                break;
            default:
                message = "operation failed";
        }
        $('#modalMessage').html(message);
        $('#myModal').modal('show');
        d.resolve();
    });

    post.fail(function (data) {
        d.reject();
    });
    return d.promise();
}

// ------------ Update Account Password ------------
$(document).ready(function () {
    $('#updatePasswordSubmitButton').on('click', function (e) {
        e.preventDefault();
        if ($('#menuPassword').validator('validate').has('.has-error').length === 0) {
            updatePassword(
                $('#oldPassword').val(),
                $('#newPassword').val(),
                $('#confirmPassword').val()
            )
        }
    });
});

function updatePassword(oldPassword, newPassword, confirmPassword) {
    var d = $.Deferred();
    var post = $.ajax({
        url: contextPath + '/public/api/updateAccountPassword',
        data: {
            oldPassword: oldPassword,
            newPassword: newPassword,
            confirmPassword: confirmPassword
        },
        method: "POST"
    });

    post.done(function (responseStatus) {
        var message;
        switch (responseStatus.status) {
            case 901:
                message = "Successfully updated";
                break;
            case 906:
                message = "wrong old password";
                break;
            case 908:
                message = "new password and confirm password do not match";
                break;
            default:
                message = "operation failed";
        }
        $('#modalMessage').html(message);
        $('#myModal').modal('show');
        d.resolve();
    });

    post.fail(function (data) {
        d.reject();
    });
    return d.promise();
}

// ------------ Update Person Info ------------
$(document).ready(function () {
    $('#menuInformation').on('click', '#updatePersonInfoSubmitButton', function (e) {
        e.preventDefault();
        if ($('#menuInformation').validator('validate').has('.has-error').length === 0) {
            updatePersonInfo();
        }
    });
});

function updatePersonInfo() {
    currentPerson["firstName"] = $('[name="firstName"]', $('#menuInformation')).val();
    currentPerson["lastName"] = $('[name="lastName"]', $('#menuInformation')).val();
    currentPerson["patronymic"] = $('[name="patronymic"]', $('#menuInformation')).val();
    currentPerson["birthday"] = $('[name="birthday"]', $('#menuInformation')).val();
    currentPerson["sex"] = $('[name="sex"]', $('#menuInformation')).val();
    currentPerson["info"] = $('[name="info"]', $('#menuInformation')).val();

    $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#myModalButton').hide();
    $('#myModal').modal('show');
    $('#modalMessage').html("Loading...");
    var post = $.ajax({
        url: contextPath + '/public/api/updatePersonInfo',
        data: currentPerson,
        method: "POST"
    });

    post.done(function (responseStatus) {
        $('#modalMessage').html(responseMessage(responseStatus));
        $('#myModalButton').show();
        loadPersonEditInfo();
    });
}

function loadPersonEditInfo() {
    $('#menuInformation').empty();
    $.post(contextPath + '/public/api/personAuth', function (person) {
        var tmp = person["birthday"];
        var day = tmp["dayOfMonth"] >= 10 ? tmp["dayOfMonth"] : "0" + tmp["dayOfMonth"];
        var month = tmp["monthValue"] >= 10 ? tmp["monthValue"] : "0" + tmp["monthValue"];
        $.get(contextPath + "/resources/templates/editAccount/editPersonInfoForm.jsp", function (infoContent) {
            $('#menuInformation').append(infoContent);
            $('#firstName').val(person["firstName"]);
            $('#lastName').val(person["lastName"]);
            $('#patronymic').val(person["patronymic"]);
            $('#birthday').val(tmp["year"] + '-' + month + '-' + day);
            $('#sex').val(person["sex"]);
            $('#info').val(person["info"]);
            currentPerson = person;

            var currentDate = new Date();
            $('[data-toggle="popover"]').popover();
            $('.date-picker').bootstrapDP({
                autoclose: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(currentDate.getFullYear() - 150, 0, 0),
                endDate: new Date(currentDate.getFullYear() - 17, currentDate.getMonth(), currentDate.getDay())
            });
            $('#menuInformation').validator('update');
        });
    });
}

// ------------ Update Person Phones ------------
$(document).ready(function () {
    $('#menuPhones').on('click', '#updatePhonesSubmitButton', function (e) {
        e.preventDefault();
        if ($('#menuPhones').validator('validate').has('.has-error').length === 0) {
            updatePersonPhones();
        }
    });

    $('#editAccountDiv').on('click', '.addPhoneButton', function (e) {
        e.preventDefault();
        var serialized = $('select', '#personPhonesForm').serializeArray();
        var index = serialized.length;
        $.get(contextPath + "/resources/templates/editAccount/editPhoneRowForm.jsp", function (phoneRow) {
            $('#personPhonesFormContent').append(phoneRow);
            $('#phoneNumber').attr("name", "number_" + index);
            $('#phoneNumber').attr("id", "number_" + index);
            $('#phoneType').attr("name", "type_" + index);
            $('#phoneType').attr("id", "type_" + index);
            $('#phoneId').attr("name", "id_" + index);
            $('#phoneId').attr("id", "id_" + index);

            $('#menuPhones').validator('update');
        });
    });
});

function objectifyEditPersonPhoneForm(formArray) {//serialize data function
    var returnArray = [];
    for (var i = 0; i < formArray.length; i++) {
        var phone = {};
        phone["id"] = formArray[i]['value'];
        phone["description"] = formArray[++i]['value'];
        phone["number"] = formArray[++i]['value'];
        phone["personEmail"] = currentPerson["email"];
        phone["personId"] = currentPerson["id"];
        returnArray.push(phone);
    }
    return returnArray;
}

function updatePersonPhones() {
    var serialized = $('input, select', '#personPhonesForm').serializeArray();
    var post = $.ajax({
        url: contextPath + '/public/api/updatePersonPhones',
        data: JSON.stringify(objectifyEditPersonPhoneForm(serialized)),
        method: "POST",
        dataType: "json",
        contentType: 'application/json'
    });
    post.done(function (responseStatus) {
        $('#modalMessage').html(responseMessage(responseStatus));
        $('#myModal').modal('show');
        loadPersonEditPhones();
    });
}

function loadPersonEditPhones() {
    $('#personPhonesFormContent').empty();
    $.post(contextPath + '/public/api/personPhones', {"id": currentPerson["id"]}, function (phones) {
        $.get(contextPath + "/resources/templates/editAccount/editPhoneRowForm.jsp", function (phoneRow) {
            $(phones).each(function (index) {
                $('#personPhonesFormContent').append(phoneRow);
                $('#phoneNumber').attr("name", "number_" + index);
                $('#phoneNumber').attr("id", "number_" + index);
                $('#phoneType').attr("name", "type_" + index);
                $('#phoneType').attr("id", "type_" + index);
                $('#phoneId').attr("name", "id_" + index);
                $('#phoneId').attr("id", "id_" + index);
                $('#number_' + index).val(phones[index]["number"]);
                $('#type_' + index).val(phones[index]["description"]);
                $('#id_' + index).val(phones[index]["id"]);
            });
            $('#personPhonesFormContent').validator('update')
        });
    });
}

// ------------ UpdatePerson Addresses ------------
$(document).ready(function () {
    $modal.on('show.bs.modal', function (e) {
        var d = $.Deferred();
        $(this)
            .addClass('modal-scrollfix')
            .find('.modal-body')
            .html("loading...")
            .load(contextPath + "/resources/templates/editAccount/editPersonAddressesForm.jsp", function (e) {
                d.resolve();
            });
        $.when(d).then(function () {
            $('#countryModal').val((addressSimple["country"]));
            $('#regionModal').val(addressSimple["region"]);
            $('#cityModal').val(addressSimple["city"]);
            $('#streetModal').val(addressSimple["street"]);
            $('#houseModal').val(addressSimple["house"]);
            $('#apartmentModal').val(addressSimple["apartment"]);
            $('#idModal').val(addressSimple["id"]);

            $('#modalAddressForm').validator('update')
        });
    });

    $modal.on('click', '#deleteAddressButton', function () {
        var serialized = $('input', $modal).serializeArray();
        var addressJSON = "{\"address\":" + JSON.stringify(objectifyPersonAddress(serialized)) + "," +
            "\"personEmail\":" + JSON.stringify(currentPerson.email) + "}";
        deletePersonAddress(addressJSON);
        $modal.modal('hide');
    });

    $modal.on('click', '#okAddressButton', function () {
        if ($('#modalAddressForm').validator('validate').has('.has-error').length === 0) {
            var serialized = $('input', $modal).serializeArray();
            var addressJSON = "{\"address\":" + JSON.stringify(objectifyPersonAddress(serialized)) + "," +
                "\"personEmail\":" + JSON.stringify(currentPerson.email) + "}";
            savePersonAddress(addressJSON);
            $modal.modal('hide');
        }
    });

    $('#addAddressButton').on('click', function (e) {
        addressSimple = {};
        addressSimple["country"] = "";
        addressSimple["region"] = "";
        addressSimple["city"] = "";
        addressSimple["street"] = "";
        addressSimple["house"] = "";
        addressSimple["apartment"] = "";
        $modal.modal('show');
    });
});

function objectifyPersonAddress(serialized) {
    var address = {};
    for (var i = 0; i < serialized.length; i++) {
        address[serialized[i]['name']] = serialized[i]['value'];
    }
    return address;
}

function deletePersonAddress(addressJSON) {
    var post = $.ajax({
        url: contextPath + '/public/api/deletePersonAddress',
        data: JSON.stringify(objectifyPersonAddress($('input', $modal).serializeArray())),
        headers: {"email": currentPerson.email},
        method: "POST",
        dataType: "json",
        contentType: 'application/json'
    });
    post.done(function (responseStatus) {
        $('#modalMessage').html(responseMessage(responseStatus));
        $('#myModal').modal('show');
        loadPersonEditAddresses();
    });
}

function savePersonAddress(addressJSON) {
    var post = $.ajax({
        url: contextPath + '/public/api/updatePersonAddress',
        data: JSON.stringify(objectifyPersonAddress($('input', $modal).serializeArray())),
        headers: {"email": currentPerson.email},
        method: "POST",
        dataType: "json",
        contentType: 'application/json'
    });
    post.done(function (responseStatus) {
        $('#modalMessage').html(responseMessage(responseStatus));
        $('#myModal').modal('show');
        loadPersonEditAddresses();
    });
}

function loadPersonEditAddresses() {
    $('#personAddressFormContent').empty();
    $.post(contextPath + '/public/api/personAddresses', {"id": currentPerson["id"]}, function (addresses) {
        $(addresses).each(function (index) {
            $('#personAddressFormContent').append(
                '<button type="button" class="btn btn-default btn-block" style="white-space: normal;" id="addressButton_' +
                addresses[index]["addressSimple"]["id"] + '">' +
                addresses[index]["addressSimple"]["country"] + ', ' +
                addresses[index]["addressSimple"]["region"] + ', ' +
                addresses[index]["addressSimple"]["city"] + ', ' +
                addresses[index]["addressSimple"]["street"] + ', ' +
                addresses[index]["addressSimple"]["house"] + ', ' +
                addresses[index]["addressSimple"]["apartment"] +
                '</button>'
            );
            $('#addressButton_' + addresses[index]["addressSimple"]["id"]).on('click', function (e) {
                e.preventDefault();
                addressSimple = addresses[index]["addressSimple"];
                $modal.modal('show');
            });
        });
    });
}

// ------------ Update Person Contacts ------------
$(document).ready(function () {
    $('#menuContacts').on('click', '#updateContactSubmitButton', function (e) {
        e.preventDefault();
        if ($('#personContactsFormContent').validator('validate').has('.has-error').length === 0) {
            updatePersonContacts();
        }
    });

    $('#editAccountDiv').on('click', '.addContactButton', function (e) {
        e.preventDefault();
        var serialized = $('input', '#personPhonesForm').serializeArray();
        var index = serialized.length / 2;
        $.get(contextPath + "/resources/templates/editAccount/editPersonContactsForm.jsp", function (contactRow) {
            $('#personContactsFormContent').append(contactRow);
            $('#contactId').attr("id", "contactId_" + index);
            $('#contactType').attr("id", "contactType_" + index);
            $('#contactValue').attr("id", "contactValue_" + index);

            $('#personContactsFormContent').validator('update')
        });
    });
});

function objectifyEditPersonContactsForm(formArray) {//serialize data function
    var returnArray = [];
    for (var i = 0; i < formArray.length; i++) {
        var contact = {};
        contact["id"] = formArray[i]['value'];
        contact["type"] = formArray[++i]['value'];
        contact["value"] = formArray[++i]['value'];
        returnArray.push(contact);
    }
    return returnArray;
}

function updatePersonContacts() {
    var serialized = $('input', '#personContactsFormContent').serializeArray();
    var contactsJSON = "{\"contacts\":" + JSON.stringify(objectifyEditPersonContactsForm(serialized)) + "," +
        "\"personEmail\":" + JSON.stringify(currentPerson.email) + "}";
    var post = $.ajax({
        url: contextPath + '/public/api/updatePersonContacts',
        data: contactsJSON,
        method: "POST",
        dataType: "json",
        contentType: 'application/json'
    });
    post.done(function (responseStatus) {
        $('#modalMessage').html(responseMessage(responseStatus));
        $('#myModal').modal('show');
        loadPersonEditContacts();
    });
}

function loadPersonEditContacts() {
    $('#personContactsFormContent').empty();
    $.post(contextPath + '/public/api/personContacts', {"id": currentPerson["id"]}, function (contact) {
        $.get(contextPath + "/resources/templates/editAccount/editPersonContactsForm.jsp", function (contactRow) {
            $(contact).each(function (index) {
                $('#personContactsFormContent').append(contactRow);
                $('#contactType').attr("id", "contactType_" + index);
                $('#contactValue').attr("id", "contactValue_" + index);
                $('#contactId').attr("id", "contactId_" + index);
                $('#contactType_' + index).val(contact[index]["type"]);
                $('#contactValue_' + index).val(contact[index]["value"]);
                $('#contactId_' + index).val(contact[index]["id"]);
            });
            $('#personContactsFormContent').validator('update')
        });
    });
}

// ------------ Update Person Avatar ------------
var newAvatarUrl;

$(document).ready(function () {
    $("#changeAvatarButton").on('click', function (e) {
        readURL($("#avatarForm").val());
        $.ajax({
                url: contextPath + '/public/api/saveAvatar',
                type: 'POST',
                data: new FormData($("#avatarForm")[0]),
                contentType: false,
                cache: false,
                processData: false,
                success: function () {
                    $('#modalMessage').html("Avatar successfully uploaded.");
                    $('#myModal').modal('show');
                }
            }
        )
    });

    $('#selectAvatar').on('click', function (e) {
        e.preventDefault();
        $('#avatarFile').click();
    });

    $("#avatarFile").change(function () {
        readURL(this);
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            newAvatarUrl = e.target.result;
            $('#avatarImg').attr('src', newAvatarUrl);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function loadPersonEditAvatar() {
    $.post(contextPath + '/public/api/personAuth', function (person) {
        currentPerson = person;
        var avatar_id = currentPerson["avatarId"];
        $('#avatarImg').attr('src', contextPath + '/public/api/getAvatar?avatar_id=' + avatar_id);
    });
}

// ------------ XML -------------
$(document).ready(function () {
    $("#importXmlFileLink").on('click', function (e) {
        e.preventDefault();
        $('#xmlFile').click();
    });

    $('#xmlFile').on('change', function (e) {
        var tmp = $('#xmlFile').val();
        if (tmp !== null && tmp !== "") {
            var post = $.ajax({
                url: contextPath + '/public/api/importPersonFromXML',
                type: 'POST',
                data: new FormData($("#importXMLForm")[0]),
                contentType: false,
                cache: false,
                processData: false
            });
            post.done(function (responseStatus) {
                $('#modalMessage').html(responseMessage(responseStatus));
                $('#myModal').modal('show');
                $('#importXMLForm')[0].reset();
            });
        }
    });
});

// ------------ Common ------------
$(document).ready(function () {
    $('#editAccountDiv').on('click', '.deleteButton', function (e) {
        e.preventDefault();
        $(this).closest('.form-inline').empty();
    });
});

function responseMessage(responseStatus) {
    switch (responseStatus.status) {
        case 900:
            return "error occurred";
        case 901:
            return "Successfully updated";
        case 902:
            return "Authentication failed";
        case 903:
            return "Invalid data. Check input.";
        default:
            return "operation failed";
    }
}

// ------------ Other ------------