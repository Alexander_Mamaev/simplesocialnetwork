$(document).ready(function () {
    var newAvatarUrl;

    $('a[href="#menu_avatar"]').on('click', function () {
        $("#changeAvatarButton").show();
    });

    $('a[href]').not('a[href="#menu_avatar"]').on('click', function () {
        $("#changeAvatarButton").hide();
    });

    $("#changeAvatarButton").on('click', function () {
        $("#avatar").click();
    });

    $("#avatar").on("change", function () {
        var file = this.files[0];
        if (file.size > 100000) {
            alert('max upload size is 100k')
        } else {
            readURL(this);

            $.ajax({
                    url: 'saveAvatarController',
                    type: 'POST',
                    data: new FormData($("#submitAvatarForm")[0]),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function () {
                        $('#avatarImg').attr('src', newAvatarUrl);
                    }
                }
            )
        }
    });

    $("#exportXMLButton").on('click', function () {
        $("#linkExportToXML")[0].click();
    });

    $("#importXMLButton").on('click', function () {
        $("#fileXML").click();
    });

    $("#fileXML").on("change", function () {
        $("#importXMLFile").click();
    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                newAvatarUrl = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
});