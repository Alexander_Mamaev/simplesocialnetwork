$(document).ready(function () {

    $('#submitButton').on('click', function () {
        if ($('#personalDataForm').valid()) {
            $('#myModalBox').modal('show');
            recountPhone();
        }
    });

    $('.check').on('click', function (e) {
        if (!$('#personalDataForm').valid()) {
            e.stopImmediatePropagation();
            recountPhone();
        }
    });

    $('#submitFormOk').on('click', function () {
        $('#personalDataForm').submit();
    });

    $("#personalDataForm").validate();

    $("[name^=phones]").each(function () {
        $(this).rules("add", {
            required: true,
            digits: true,
            minlength: 11,
            maxlength: 11
        });
    });

    $('[name=name]').each(function () {
        $(this).rules("add", {
            required: true,
            pattern: /^[а-яА-ЯёЁa-zA-Z_0-9]+$/
        });
    });

    $('[name=sex]').each(function () {
        $(this).rules("add", {
            required: true,
            pattern: /[M, F]/
        });
    });

    $('#datepicker').each(function () {
        $(this).rules("add", {
            required: true
        })
    });

    function recountPhone() {
        var counter = 0;
        $('.phoneNumber').each(function () {
            $(this).attr('name', 'phones[' + counter + '].number');
            counter++;
        });
        counter = 0;
        $('.phoneType').each(function () {
            $(this).attr('name', 'phones[' + counter + '].type');
            counter++;
        });
    }
});
