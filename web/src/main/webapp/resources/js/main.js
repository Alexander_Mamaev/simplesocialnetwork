var userName;
var personId;
var sizeOfSearch = 5;

$(document).ready(function () {
    var sessionExpiredAlertCounter = 0;

    $.fn.bootstrapDP = $.fn.datepicker.noConflict(); // need for no conflict between jquery datepicker and bootstrap one
    var currentDate = new Date();
    $('[data-toggle="popover"]').popover();
    $('#datepicker').bootstrapDP({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate: new Date(currentDate.getFullYear() - 150, 0, 0),
        endDate: new Date(currentDate.getFullYear() - 17, currentDate.getMonth(), currentDate.getDay())
    });
    homeContent();

    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('ajax', "true");
            xhr.setRequestHeader('X-CSRF-TOKEN', csrf_token);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.getResponseHeader('ajax_error') !== null) {
                if (sessionExpiredAlertCounter === 0) {
                    alert(jqXHR.getResponseHeader('ajax_error'));
                    sessionExpiredAlertCounter += 1;
                }
                window.location = contextPath + "/public/main";
            }
        }
    });
});

function loadEditAccountLinkContent() {
    // $('#dynamic-content').empty();
    $('#dynamic-content').html("loading...");
    $('#dynamic-content').load(contextPath + '/public/editAccount', function (responseText, textStatus, jqXHR) {
        $('#accountUsername').val(userName);
    })
}

function homeContent() {
    // $('#dynamic-content').empty();
    $('#dynamic-content').html("loading...");
    loadHomeContent(personId);
}

function chatContent() {
    // $('#dynamic-content').empty();
    $('#dynamic-content').html("loading...");
    loadChatContent();
}

function loadHomeContent(personId) {
    $('#dynamic-content').load(contextPath + '/public/homeContent', function (responseText, textStatus, jqXHR) {
        if (jqXHR.status === 200) {
            $.when(
                loadPersonInfo(personId))
                .done(function (person) {
                    loadPersonAvatar(person["avatarId"]);
                    loadPersonPhones(person["id"]);
                    loadPersonAddresses(person["id"]);
                    loadPersonContacts(person["id"]);
                })
        } else {
            window.location.replace(contextPath);
        }

        $.post(contextPath + '/public/api/checkFriend', {personId: personId}, function (data) {
            if (data === false) {
                $('#becomeFriendsButton').show();
            }
        });

        $('#becomeFriendsButton').on('click', function (e) {
            e.preventDefault();
            $.post(contextPath + '/public/api/requestFriend', {personId: personId}, function (data) {
                $('#becomeFriendsButton').hide();
            });
        });
    });
}

function loadSearchContent(searchQuery, page, size) {
    $('#dynamic-content').html("loading...");
    $('#dynamic-content').load(contextPath + '/public/searchContent', function (responseText, textStatus, jqXHR) {
        $.post(contextPath + '/public/api/countPersonByQuery', {searchQuery: searchQuery}, function (data) {
            $('#personsList').append(
                '<div class="text-center">' +
                '<p style="color: #507299">found ' + data + ' persons</p>' +
                '</div>'
            );
            loadSearchResults(searchQuery, page, size);
        });
        $('#morePersons').on('click', function (e) {
            e.preventDefault();
            page += 1;
            loadSearchResults(searchQuery, page, size);
        })
    });
}

function loadSearchResults(searchQuery, page, size) {
    $.post(contextPath + '/public/api/searchPersonsWithQuery', {searchQuery: searchQuery, page: page, size: size},
        function (data) {
            if (data.length < size) {
                $('#morePersons').hide();
            }

            $.each(data, function (i, element) {
                $('#personsList').append(
                    '<form class="form-inline" id="form-inline' + element.id + '">' +
                    '  <button type="button" class="btn btn-default form-group" name="' + element.id + 'person" id="' + element.id + '">' +
                    '    <div class="media">' +
                    '      <div class="media-left">' +
                    '        <img src="' + contextPath + '/public/api/getAvatar?avatar_id=' + element.avatarId + '" class="media-object" style="width:50px; max-height: 50px">' +
                    '      </div>' +
                    '      <div class="media-body">' +
                    '        <h4 class="media-heading">' + element.lastName + '</h4>' +
                    '        <p>' + element.firstName + ' ' + element.patronymic + '</p>' +
                    '      </div>' +
                    '    </div>' +
                    '  </button>' +
                    '</div>'
                );

                $('.btn[name=' + element.id + 'person]').on('click', function (e) {
                    e.preventDefault();
                    loadHomeContent(element.id);
                });
            });
        });
}

function loadChatContent() {
    chatPage = 0;
    $('#dynamic-content').load(contextPath + '/public/templates/common_chat', function () {
        chatConnect();
        $('#messageTextArea').keydown(enterKeyUpdater);
        pagingMessages(chatPage++, messagesForOnePage);
        scrollUpMessageListener();
    });
}

/**
 * Load Person by email or id (parse parameter by '@')
 */
function loadPersonInfo(personId) {
    var d = $.Deferred();
    var post = $.ajax({
        url: contextPath + '/public/api/personInfoById',
        data: {
            id: personId
        },
        method: "POST"
    });

    post.done(function (data) {
        var tmp = data["birthday"];
        var day = tmp["dayOfMonth"] >= 10 ? tmp["dayOfMonth"] : "0" + tmp["dayOfMonth"];
        var month = tmp["monthValue"] >= 10 ? tmp["monthValue"] : "0" + tmp["monthValue"];
        var info = data["info"] === null ? "" : data["info"].replace(/\r?\n/g, "<br>");
        var personInfo =
            '<tr><td>First name:</td><td>' + data["firstName"] + '</td></tr>' +
            '<tr><td>Last name:</td><td>' + data["lastName"] + '</td></tr>' +
            '<tr><td>Patronymic:</td><td>' + data["patronymic"] + '</td></tr>' +
            '<tr><td>Email:</td><td>' + data["email"] + '</td></tr>' +
            '<tr><td>Birthday:</td><td>' + day + '/' + month + '/' + tmp["year"] + '</td></tr>' +
            '<tr><td>Sex:</td><td>' + data["sex"] + '</td></tr>' +
            '<tr><td>Info:</td><td>' + info + '</td></tr>';
        $('#personalInfo').append(personInfo);
        d.resolve(data);
    });

    post.fail(function () {
        d.reject();
    });
    return d.promise();
}

function loadPersonPhones(person_id) {
    var d = $.Deferred();
    var post = $.ajax({
        url: contextPath + '/public/api/personPhones',
        data: {
            id: person_id
        },
        method: "POST"
    });

    post.done(function (data) {
        $(data).each(function (index) {
            var phoneRow = '<tr><td>' + data[index]["description"] + ':</td><td>' + data[index]["number"] + '</td></tr>';
            $('#phones').append(phoneRow);
        });
        d.resolve(data);
    });

    post.fail(function () {
        d.reject();
    });
    return d.promise();
}

function loadPersonAddresses(person_id) {
    var d = $.Deferred();
    var post = $.ajax({
        url: contextPath + '/public/api/personAddresses',
        data: {
            id: person_id
        },
        method: "POST"
    });

    post.done(function (data) {
        $(data).each(function (index) {
            var tmp = data[index]["addressSimple"];
            var addressRow =
                '<tr>' +
                // '<td>' + data[index]["description"] + ':</td>' +                     //TODO description not implemented yet
                '<td>' + tmp["country"] + ' ' + tmp["region"] + ' ' + tmp["city"] + ' ' +
                tmp["street"] + ' ' + tmp["house"] + ' ' + tmp["apartment"] + '</td>' +
                '</tr>';
            $('#addresses').append(addressRow);
            d.resolve(data);
        })
    });

    post.fail(function () {
        d.reject();
    });
    return d.promise();
}

function loadPersonContacts(person_id) {
    var d = $.Deferred();
    var post = $.ajax({
        url: contextPath + '/public/api/personContacts',
        data: {
            id: person_id
        },
        method: "POST"
    });

    post.done(function (data) {
        $(data).each(function (index) {
            var contactRow = '<tr><td>' + data[index]["type"] + ':</td><td>' + data[index]["value"] + '</td></tr>';
            $('#contacts').append(contactRow);
            d.resolve(data);
        })
    });

    post.fail(function () {
        d.reject();
    });
    return d.promise();
}

function loadPersonAvatar(avatar_id) {
    var avatarRow = '<img class="img-responsive img-rounded center-block" src=' +
        contextPath + '/public/api/getAvatar?avatar_id=' + avatar_id + ' style="max-height: 130px; max-width: 130px">';
    $('#avatar').append(avatarRow);
}

function loadScript(filePathToJSScript) {
    var list = document.getElementsByTagName('script');
    var i = list.length, flag = false;
    while (i--) {
        if (list[i].src.indexOf(filePathToJSScript) >= 0)
            flag = true;
        break;
    }

// if we didn't already find it on the page, add it
    if (!flag) {
        var tag = document.createElement('script');
        tag.src = filePathToJSScript;
        document.getElementsByTagName('body')[0].appendChild(tag)
    }
}
