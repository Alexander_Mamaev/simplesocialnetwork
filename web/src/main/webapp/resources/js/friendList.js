function friendsContent() {
    $('#dynamic-content').html("loading...");
    $('#dynamic-content').load(contextPath + '/public/friendsContent', function (responseText, textStatus, jqXHR) {
        loadFriendList();
    });
}

function loadFriendList() {
    $('#friendsList').html("loading...");
    $.post(contextPath + '/public/api/personFriends', function (data) {
        fillFriendContent('#friendsList', data);
    });
}

function incomeContent() {
    $('#incomeList').html("loading...");
    $.post(contextPath + '/public/api/incomeFriendRequest', function (data) {
        fillFriendContent('#incomeList', data);
    });
}

function outcomeContent() {
    $('#outcomeList').html("loading...");
    $.post(contextPath + '/public/api/outcomeFriendRequest', function (data) {
        fillFriendContent('#outcomeList', data);
    });
}

function fillFriendContent(tagId, data) {
    $(tagId).empty();
    $.each(data, function (i, element) {
        $(tagId).append(
            '<form class="form-inline" id="form-inline' + element.id + '">' +
            '  <button type="button" class="btn btn-default form-group" name="' + element.id + 'person" id="' + element.id + '" style="max-width: 90%">' +
            '    <div class="media">' +
            '      <div class="media-left">' +
            '        <img src="' + contextPath + '/public/api/getAvatar?avatar_id=' + element.avatarId + '" class="media-object" style="width:50px; max-height: 50px">' +
            '      </div>' +
            '      <div class="media-body">' +
            '        <h4 class="media-heading">' + element.lastName + '</h4>' +
            '        <p>' + element.firstName + ' ' + element.patronymic + '</p>' +
            '      </div>' +
            '    </div>' +
            '  </button>' +
            '  <div id="deleteButton' + element.id + '" class="form-group custom-buttons-1 deleteButton">' +
            '    <small>Delete</small>' +
            '  </div>' +
            '</div>'
        );

        $('.btn[name=' + element.id + 'person]').on('click', function (e) {
            e.preventDefault();
            loadHomeContent(element.id);
        });

        $('#deleteButton' + element.id).on('click', function (e) {
            $.post(contextPath + '/public/api/deleteFriendsAndRequests', {friendId: element.id}, function (data) {
                $('#form-inline' + element.id).remove();
            });
        });
    });
}