<%@page pageEncoding="UTF-8" %>
<form id="modalAddressForm" name="modalAddresForm" data-toggle="validator">
    <input type="hidden" id="idModal" name="id">
    <div class="form-group has-feedback">
        <label class="text-info" for="countryModal">Country</label>
        <input class="form-control" id="countryModal" name="country"
               data-minlength="2"
               pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
               data-error="Letters and spaces are allowed."
               required>
        <span class="glyphicon form-control-feedback"></span>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
        <label class="text-info" for="regionModal">Region</label>
        <input class="form-control" id="regionModal" name="region"
               pattern="^[a-zA-Zа-яА-ЯёЁ0-9\s]+$"
               data-error="Letters, numbers and spaces are allowed."
               required>
        <span class="glyphicon form-control-feedback"></span>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
        <label class="text-info" for="cityModal">City</label>
        <input class="form-control" id="cityModal" name="city"
               pattern="^[a-zA-Zа-яА-ЯёЁ0-9\s]+$"
               data-error="Letters, numbers and spaces are allowed."
               required>
        <span class="glyphicon form-control-feedback"></span>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
        <label class="text-info" for="streetModal">Street</label>
        <input class="form-control" id="streetModal" name="street"
               pattern="^[a-zA-Zа-яА-ЯёЁ0-9\s]+$"
               data-error="Letters, numbers and spaces are allowed."
               required>
        <span class="glyphicon form-control-feedback"></span>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
        <label class="text-info" for="houseModal">House</label>
        <input class="form-control" id="houseModal" name="house"
               pattern="^[a-zA-Zа-яА-ЯёЁ0-9\s/]+$"
               data-error="Letters, numbers, / and spaces are allowed."
               required>
        <span class="glyphicon form-control-feedback"></span>
        <div class="help-block with-errors"></div>
    </div>
    <div class="form-group has-feedback">
        <label class="text-info" for="apartmentModal">Apartment</label>
        <input class="form-control" id="apartmentModal" name="apartment"
               pattern="^[a-zA-Zа-яА-ЯёЁ0-9\s/]+$"
               data-error="Letters, numbers, / and spaces are allowed."
               required>
        <span class="glyphicon form-control-feedback"></span>
        <div class="help-block with-errors"></div>
    </div>
    <div style="text-align: right">
        <button type="button" class="btn btn-primary" id="okAddressButton">Ok</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="deleteAddressButton">Delete</button>
    </div>
</form>