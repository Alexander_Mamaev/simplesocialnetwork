<form class="form-inline">
    <input type="hidden" id="contactId" name="contactId">
    <div class="form-group has-feedback">
        <input class="form-control" id="contactType" name="type" style="width: 100px"
               required>
        <span class="glyphicon form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input class="form-control" id="contactValue" name="value"
               required>
        <span class="glyphicon form-control-feedback"></span>
    </div>
    <div class="form-group">
        <div id="deleteContactButton" class="form-group custom-buttons-1 deleteButton">
            <small>Delete</small>
        </div>
    </div>
</form>