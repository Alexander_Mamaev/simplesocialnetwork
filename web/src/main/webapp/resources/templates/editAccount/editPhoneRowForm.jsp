<form class="form-inline">
    <input type="hidden" name="id" id="phoneId">
    <div class="form-group has-feedback">
        <select class="form-control" name="type" id="phoneType">
            <option>Personal</option>
            <option>Work</option>
            <option>Home</option>
            <option>Other</option>
        </select>
    </div>
    <div class="form-group has-feedback">
        <input class="form-control" name="number" id="phoneNumber" placeholder="+79991234567"
               pattern="^\+\d{11}"
               required>
        <span class="glyphicon form-control-feedback"></span>
    </div>
    <div id="deletePhoneButton" class="form-group custom-buttons-1 deleteButton">
        <small>Delete</small>
    </div>
</form>