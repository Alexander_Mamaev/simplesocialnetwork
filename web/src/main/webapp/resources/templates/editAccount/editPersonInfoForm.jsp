<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<form id="personInfoForm" name="personInfoForm" data-toggle="validator">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="form-group has-feedback">
            <label class="text-info" for="firstName">First name</label>
            <input class="form-control" name="firstName" id="firstName"
                   pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
                   required>
            <span class="glyphicon form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            <label class="text-info" for="lastName">Last name</label>
            <input class="form-control" name="lastName" id="lastName"
                   pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
                   required>
            <span class="glyphicon form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            <label class="text-info" for="patronymic">Patronymic</label>
            <input class="form-control" name="patronymic" id="patronymic"
                   pattern="^[a-zA-Zа-яА-ЯёЁ\s]+$"
                   required>
            <span class="glyphicon form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            <label class="text-info" for="birthday">Birthday</label>
            <input class="form-control date-picker" id="birthday" name="birthday"
                   pattern="^\d{4}-\d{2}-\d{2}$"
                   placeholder="YYYY-MM-DD"
                   required>
            <span class="glyphicon form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>
        <div class="form-group has-feedback">
            <label class="text-info" for="sex">Sex</label>
            <select class="form-control" id="sex" name="sex"
                    required>
                <c:choose>
                    <c:when test="${dto.sex == 'F'}">
                        <option value="M">Male</option>
                        <option value="F" selected>Female</option>
                    </c:when>
                    <c:otherwise>
                        <option value="M" selected>Male</option>
                        <option value="F">Female</option>
                    </c:otherwise>
                </c:choose>
            </select>
        </div>
        <div class="form-group has-feedback">
            <label class="text-info" for="info">Info</label>
            <textarea class="form-control" rows="5" name="info" id="info"></textarea>
            <span class="glyphicon form-control-feedback"></span>
            <div class="help-block with-errors"></div>
        </div>
        <input name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
        <div class="text-center">
            <button class="btn btn-default" id="updatePersonInfoSubmitButton" style="margin-top:10px">
                <span class="text-info">Save</span>
            </button>
        </div>
    </div>
</form>