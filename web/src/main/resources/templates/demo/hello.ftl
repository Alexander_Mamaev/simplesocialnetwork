<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello freemarker</title>
</head>
<body>
<h2>Hello Freemarker</h2>
<#if integer_list??>
    <#list integer_list as number>
    <p>${number}</p>
    </#list>
</#if>

</body>
</html>