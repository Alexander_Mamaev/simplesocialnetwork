package com.getjavajob.training.web1609.mamaeva.web.api;

import com.getjavajob.training.web1609.mamaeva.common.account.Account;
import com.getjavajob.training.web1609.mamaeva.common.message.StatusMessage;
import com.getjavajob.training.web1609.mamaeva.common.person.Person;
import com.getjavajob.training.web1609.mamaeva.common.person.Sex;
import com.getjavajob.training.web1609.mamaeva.service.PersonService;
import com.getjavajob.training.web1609.mamaeva.web.App;
import com.getjavajob.training.web1609.mamaeva.web.util.WebUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = {PersonApi.class, App.class})
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:integration_test.properties")
@WithMockUser(password = "2@mail.ru", value = "2@mail.ru", roles = "USER")
public class PersonApiTest {
    @Autowired
    private PersonService personService;
    @Autowired
    private MockMvc mvc;
    @MockBean
    private WebUtil webUtil;
    private Person person;

    @Before
    public void before() {
        person = Person.builder()
                .email("2@mail.ru")
                .firstName("firstName2")
                .lastName("lastName1")
                .patronymic("patronymic2")
                .birthday(LocalDate.of(2002, 2, 2))
                .sex(Sex.M)
                .info("info2")
                .build();
    }

    @Test
    @WithMockUser(password = "1@mail.ru", value = "1@mail.ru", roles = "ADMIN")
    public void getPublicAccount() throws Exception {
        mvc.perform(get("/public/api/persons/1").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=utf-8"))
                .andExpect(content().string("1@mail.ru"))
                .andDo(print())
        ;
    }

    @Test
    public void personAuth() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        MvcResult mvcResult = mvc.perform(post("/public/api/personAuth").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andReturn();
        String expected = "{\"firstName\":\"firstName2\",\"lastName\":\"lastName2\",\"patronymic\":\"patronymic2\",\"email\":\"2@mail.ru\",";
        assertThat(mvcResult.getResponse().getContentAsString(), containsString(expected));
    }

    @Test
    public void personInfo() throws Exception {
        MvcResult mvcResult = mvc.perform(post("/public/api/personInfo")
                .param("email", "1@mail.ru").with(csrf()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
        String expected = "{\"firstName\":\"firstName1\",\"lastName\":\"lastName1\",\"patronymic\":\"patronymic1\",\"email\":\"1@mail.ru\",";
        assertThat(mvcResult.getResponse().getContentAsString(), containsString(expected));
    }

    @Test
    public void personPhones() throws Exception {
        String expected = "[{\"number\":\"+71111111111\",\"description\":\"Personal\",\"id\":1},{\"number\":\"+61111111111\",\"description\":\"Work\",\"id\":2},{\"number\":\"+51111111111\",\"description\":\"Home\",\"id\":3},{\"number\":\"+41111111111\",\"description\":\"Other\",\"id\":4}]";
        mvc.perform(post("/public/api/personPhones")
                .param("id", "1").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(expected))
                .andDo(print())
        ;
    }

    @Test
    public void personAddresses() throws Exception {
        String expected = "[{\"addressSimple\":{\"country\":\"country1\",\"region\":\"region1\",\"city\":\"city1\",\"street\":\"street1\",\"house\":\"house1\",\"apartment\":\"apartment1\",\"id\":1},\"description\":\"work\"},{\"addressSimple\":{\"country\":\"country2\",\"region\":\"region2\",\"city\":\"city2\",\"street\":\"street2\",\"house\":\"house2\",\"apartment\":\"apartment2\",\"id\":2},\"description\":\"home\"},{\"addressSimple\":{\"country\":\"country3\",\"region\":\"region3\",\"city\":\"city3\",\"street\":\"street3\",\"house\":\"house3\",\"apartment\":\"apartment3\",\"id\":3},\"description\":\"other\"}]";
        mvc.perform(post("/public/api/personAddresses")
                .param("id", "1").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(expected))
                .andDo(print())
        ;
    }

    @Test
    public void personContacts() throws Exception {
        String expected = "[{\"value\":\"dop1@mail.ru\",\"type\":\"email\",\"id\":1},{\"value\":\"skype1\",\"type\":\"skype\",\"id\":2},{\"value\":\"icq1\",\"type\":\"icq\",\"id\":3},{\"value\":\"telegram1\",\"type\":\"telegram\",\"id\":4},{\"value\":\"viber1\",\"type\":\"viber\",\"id\":5},{\"value\":\"watsapp1\",\"type\":\"watsapp\",\"id\":6}]";
        mvc.perform(post("/public/api/personContacts")
                .param("id", "1").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(expected))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonInfo_code_3() throws Exception {
        mvc.perform(post("/public/api/updatePersonInfo")
                .param("id", "1").with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.INCORRECT_DATA.toJson()))
                .andDo(print()).andReturn();
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonInfo_code_2() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("1@mail.ru").build());
        PersonService personService = mock(PersonService.class);
        when(personService.saveAndFlush(person)).thenReturn(person);
        mvc.perform(post("/public/api/updatePersonInfo")
                .flashAttr("person", person).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.AUTH_FAILED.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonInfo_code_1() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        Person person = personService.findPersonByEmail("2@mail.ru");
        person.setInfo("info222");
        mvc.perform(post("/public/api/updatePersonInfo")
                .flashAttr("person", person).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.SUCCESS.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonPhones_code_3() throws Exception {
        String json = "[{\"number\":\"+71\",\"description\":\"P\",\"id\":1}]";
        mvc.perform(post("/public/api/updatePersonPhones")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.INCORRECT_DATA.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonPhones_code_2() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "[{\"number\":\"+71111111111\",\"description\":\"Personal\",\"id\":1, \"personEmail\":\"email\"}]";
        mvc.perform(post("/public/api/updatePersonPhones")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.AUTH_FAILED.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonPhones_code_1() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "[{\"number\":\"+71111111111\",\"description\":\"Personal\",\"id\":1}]";
        mvc.perform(post("/public/api/updatePersonPhones")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.SUCCESS.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonContacts_code_3() throws Exception {
        String json = "{\"contacts\":[{\"id\":\"1\",\"type\":\"email\",\"value\":\"\"}],\"personEmail\":\"1@mail.ru\"}";
        mvc.perform(post("/public/api/updatePersonContacts")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(json).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.INCORRECT_DATA.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonContacts_code_2() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "{\"contacts\":[{\"id\":\"1\",\"type\":\"email\",\"value\":\"dop1@mail.ru\"},{\"id\":\"2\",\"type\":\"skype\",\"value\":\"skype1\"},{\"id\":\"3\",\"type\":\"icq\",\"value\":\"icq1\"},{\"id\":\"4\",\"type\":\"telegram\",\"value\":\"telegram1\"},{\"id\":\"5\",\"type\":\"viber\",\"value\":\"viber1\"},{\"id\":\"6\",\"type\":\"watsapp\",\"value\":\"watsapp1\"}],\"personEmail\":\"1@mail.ru\"}";
        mvc.perform(post("/public/api/updatePersonContacts")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.AUTH_FAILED.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonContacts_code_1() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("1@mail.ru").build());
        String json = "{\"contacts\":[{\"id\":\"1\",\"type\":\"email\",\"value\":\"dop1@mail.ru\"},{\"id\":\"2\",\"type\":\"skype\",\"value\":\"skype1\"},{\"id\":\"3\",\"type\":\"icq\",\"value\":\"icq1\"},{\"id\":\"4\",\"type\":\"telegram\",\"value\":\"telegram1\"},{\"id\":\"5\",\"type\":\"viber\",\"value\":\"viber1\"},{\"id\":\"6\",\"type\":\"watsapp\",\"value\":\"watsapp1\"}],\"personEmail\":\"1@mail.ru\"}";
        mvc.perform(post("/public/api/updatePersonContacts")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json).with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.SUCCESS.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonAddress_code_3() throws Exception {
        String json = "{\"id\":\"13\",\"country\":\"country1\",\"region\":\"region1\",\"city\":\"city1\",\"street\":\"street1\",\"house\":\"house1\",\"apartment\":\"apartment1\"}";
        mvc.perform(post("/public/api/updatePersonAddress")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)
                .header("email", "2@mail.ru")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.INCORRECT_DATA.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonAddress_code_2() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "{\"id\":\"13\",\"country\":\"country\",\"region\":\"region1\",\"city\":\"city1\",\"street\":\"street1\",\"house\":\"house1\",\"apartment\":\"apartment1\"}";
        mvc.perform(post("/public/api/updatePersonAddress")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)
                .header("email", "1@mail.ru")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.AUTH_FAILED.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void updatePersonAddress_code_1() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "{\"id\":\"1\",\"country\":\"country\",\"region\":\"region1\",\"city\":\"city1\",\"street\":\"street1\",\"house\":\"house1\",\"apartment\":\"apartment1\"}";
        String expected = StatusMessage.SUCCESS.toString();
        mvc.perform(post("/public/api/updatePersonAddress")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)
                .header("email", "2@mail.ru")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.SUCCESS.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void deletePersonAddress_code_2() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "{\"id\":\"1\",\"country\":\"country\",\"region\":\"region1\",\"city\":\"city1\",\"street\":\"street1\",\"house\":\"house1\",\"apartment\":\"apartment1\"}";
        mvc.perform(post("/public/api/updatePersonAddress")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)
                .header("email", "1@mail.ru")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.AUTH_FAILED.toJson()))
                .andDo(print())
        ;
    }

    @Test
    @Transactional
    @Rollback
    public void deletePersonAddress_code_1() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String json = "{\"id\":\"1\",\"country\":\"country\",\"region\":\"region1\",\"city\":\"city1\",\"street\":\"street1\",\"house\":\"house1\",\"apartment\":\"apartment1\"}";
        mvc.perform(post("/public/api/updatePersonAddress")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json)
                .header("email", "2@mail.ru")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(StatusMessage.SUCCESS.toJson()))
                .andDo(print())
        ;
    }

    @Test
    public void exportPersonToXML() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        String expected = fileToStringConverter("/exportPersonToXML.xml");
        MvcResult result = mvc.perform(get("/public/api/exportPersonToXml")
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/xml;charset=UTF-8"))
                .andExpect(header().string("Content-Disposition", "attachment; filename=person.xml"))
                .andDo(print())
                .andReturn();
        assertThat(result.getResponse().getContentAsString().replace("\n", "").replace("\r", ""),
                equalTo(expected.replace("\n", "").replace("\r", "")));
    }

    @Test
    @Transactional
    @Rollback
    public void importPersonFromXML() throws Exception {
        when(webUtil.getAuthAccount()).thenReturn(Account.builder().username("2@mail.ru").build());
        MockMultipartFile firstFile = new MockMultipartFile("file", "person.txt", "text/plain",
                fileToStringConverter("/importPersonFromXML.xml").getBytes());
        mvc.perform(MockMvcRequestBuilders.fileUpload("/public/api/importPersonFromXML")
                .file(firstFile)
                .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(StatusMessage.SUCCESS.toJson()))
                .andDo(print())
        ;
        Person person = personService.findPersonByEmail("2@mail.ru");
        assertThat(person.getFirstName(), equalTo("firstName"));
    }

    @SuppressWarnings("Duplicates")
    private String fileToStringConverter(String fileName) throws IOException {
        try (InputStream is = getClass().getResourceAsStream(fileName);
             ByteArrayOutputStream result = new ByteArrayOutputStream()) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF-8");
        } catch (IOException e) {
            throw new IOException(e.getMessage(), e);
        }
    }
}